package avrora.sim.types;

import avrora.Defaults;
import avrora.Main;
import avrora.core.*;
import avrora.sim.*;
import avrora.sim.platform.MicaZ_MultiCore;
import avrora.sim.platform.Platform;
import avrora.sim.platform.PlatformFactory;
import avrora.sim.platform.sensors.*;
import avrora.sim.radio.*;
import avrora.sim.clock.RippleSynchronizer;
import cck.text.StringUtil;
import cck.util.*;

import java.io.IOException;
import java.util.*;

/**
 * The <code>SensorSimulation</code> class represents a simulaion type where multiple sensor nodes,
 * each with a microcontroller, sensors, and a radio, are run in parallel. It supports options from
 * the command line that allow a simulation to be constructed with multiple nodes with multiple different
 * programs.
 *
 * @author Ben L. Titzer
 */
public class Multi_Core_Sensor_Simulation extends Simulation {

    public static String HELP = "The sensor network simulation is used for simulating multiple sensor nodes " +
            "simultaneously. These nodes can communicate with each other wirelessly to exchange packets that " +
            "include sensor data and routing information for a multi-hop network. Currently, only the \"mica2\" " +
            "and \"micaz\" platform sensor nodes are supported.";

    public final Option.List NODECOUNT = newOptionList("nodecount", "1",
            "This option is used to specify the number of nodes to be instantiated. " +
            "The format is a list of integers, where each integer specifies the number of " +
            "nodes to instantiate with each program supplied on the command line. For example, " +
            "when set to \"1,2\" one node will be created with the first program loaded onto it, " +
            "and two nodes created with the second program loaded onto them.");
    public final Option.Str TOPOLOGY = newOption("topology", "",
            "This option can be used to specifcy to topology (static, rwp). " +
            "When this option is specified, the radius or free-space radio model will be used " +
            "to model radio propagation.");
    public final Option.Bool LOSSY_MODEL = newOption("lossy-model",false,
            "When this option is set, the radio model takes into account Noise and fadings thus " +
            "implementing in micaz platform the correlation, cca and rssi functions.");
    public final Option.Str NOISE = newOption("Noise", "",
            "This option can be used to specify the name of " +
            "a file that contains a Noise time trace. When this option is specified" +
            "the indoor radio model will be used to model radio propagation.");
    public final Option.Double RANGE = newOption("radio-range", 15.0,
            "This option, when used in conjunction with the -topology option, specifies " +
            "the maximum range for radio communication between nodes. This simple " +
            "idealized radius model will drop all communications between nodes whose " +
            "distance is greater than this threshold value.");
//Ben On    
//Original    
    public final Option.Interval RANDOM_START = newOption("random-start", 0, 0,
//Alternative
//    public final Option.Interval RANDOM_START = newOption("random-start", 0, 100,
//Ben Off            
            "This option inserts a random delay before starting " +
            "each node in order to prevent artificial cycle-level synchronization. The " +
            "starting delay is pseudo-randomly chosen with uniform distribution over the " +
            "specified interval, which is measured in clock cycles. If the \"random-seed\" " +
            "option is set to a non-zero value, then its value is used as the seed to the " +
            "pseudo-random number generator.");
    public final Option.Long STAGGER_START = newOption("stagger-start", 0,
            "This option causes the simulator to insert a progressively longer delay " +
            "before starting each node in order to avoid artificial cycle-level " +
            "synchronization between nodes. The starting times are staggered by the specified number " +
            "of clock cycles. For example, if this option is given the " +
            "value X, then node 0 will start at time 0, node 1 at time 1*X, node 2 at " +
            "time 2*X, etc.");
    public final Option.List SENSOR_DATA = newOptionList("sensor-data", "",
            "This option accepts a list describing the input data for each sensor node. The format " +
            "for each entry in this list is $sensor:$id:$data, where $sensor is the name of " +
            "the sensor device such as \"light\", $id is the integer ID of the node, and $data is " +
            "the name of a file or the special '.' character, indicating random data. A sensor data " +
            "input file consists of an initial sensor reading which is interpreted as a 10-bit ADC " +
            "result, then a list of time value pairs separated by whitespace; the sensor will continue " +
            "returning the current value until the next (relative) time in seconds, and then the sensor " +
            "will change to the new value. ");
    public final Option.Bool UPDATE_NODE_ID = newOption("update-node-id", true,
            "When this option is set, the sensor network simulator will attempt to update " +
            "the node identifiers stored in the flash memory of the program. For TinyOS programs, " +
            "this identifier is labelled \"TOS_LOCAL_ADDRESS\". For SOS programs, this identifier is " +
            "called \"node_address\". When loading a program onto " +
            "a node, the simulator will search for these labels, and if found, will update the word " +
            "in flash with the node's ID number.");

    class SensorDataInput {
        String sensor;
        String fname;

        void instantiate(Platform p) {
            try {
                Sensor s = (Sensor)p.getDevice(sensor+"-sensor");
                
                if ( s == null )
                    Util.userError("Sensor device does not exist", sensor);
                
                if ( ".".equals(fname) ) 
                    s.setSensorData(new RandomSensorData(getRandom()));
                else 
                    s.setSensorData(new ReplaySensorData(p.getMicrocontroller(), fname));
            } catch ( IOException e) {
                throw Util.unexpected(e);
            }
        }
    }

    /**
     * The <code>Multi_Core_Sensor_Node</code> class extends the <code>Node</code> class of a simulation
     * by adding a reference to the radio device as well as sensor data input. It extends the
     * <code>instantiate()</code> method to create a new thread for the node and to attach the
     * sensor data input.
     */
    protected class Multi_Core_Sensor_Node extends Node 
    {
        Radio                   radio;
        long[]                  startups;
//        long                    startup;
        List<SensorDataInput>   sensorInput;

        Multi_Core_Sensor_Node(int                  id, 
                                PlatformFactory     pf, 
                                LoadableProgram[]   progs) 
        {
            super(id, pf, progs);
            sensorInput = new LinkedList<SensorDataInput>();
        }
        
        /**
         * The <code>instantiate()</code> method of the sensor node extends the default simulation node
         * by creating a new thread to execute the node as well as getting references to the radio and
         * adding it to the radio model, adding
         * an optional start up delay for each node, and connecting the node's sensor input to
         * replay or random data as specified on the command line.
         */
        protected void instantiate() 
        {
            Setup_Node();
            updateNodeID();
            addSensorData();
        }
        
        /***********************************************************************
         * 
         **********************************************************************/
        private void addSensorData() 
        {
            // process sensor data inputs
            for (SensorDataInput sdi : sensorInput) 
                sdi.instantiate(platform);
        }

        
  /*      protected void instantiate() {
            // create the simulator object
            platform = platformFactory.newPlatform( id, Simulation.this, path.getProgram() );

            simulators = new Simulator[ platform.getCores().size() ];

            int sim_cnt = 0;
            for ( Microcontroller m : platform.getCores().values() )
            {    
                simulators[sim_cnt] = m.getSimulator();
                sim_cnt++;
            }
            
            simulator = simulators[0];
         
            
            processTimeout();
            processInterruptSched();
            processEepromLoad();
            synchronizer.addNode(this);
        }        */
        
        /***********************************************************************
         * 
         **********************************************************************/
        private void Setup_Node() 
        {
            super.Instantiate_Multicore();
            
            // get the radio device, if it exists.
            Object dev = platform.getDevice("radio");
            if (dev instanceof CC2420Radio) 
            {
                // connect to the cc2420 medium
                radio = (CC2420Radio)dev;
                radio.setMedium( createCC2420Medium() );
            } 
            else if (dev instanceof CC1000Radio) 
            {
                // connect to the cc1000 medium
                radio = (CC1000Radio)dev;
                radio.setMedium( createCC1000Medium() );
            }
            
            for ( short i = 0; i < simulators.length; i++ )
                simulators[i].delay( startups[i] );
            
            if (topology != null) 
                setNodePosition();
        }

        
        /***********************************************************************
         * 
         **********************************************************************/        
        private Medium createCC2420Medium() 
        {
            if (cc2420_medium != null)
                return cc2420_medium;
            
            createRadioModel();
            
            if ( LOSSY_MODEL.get() )
                return cc2420_medium = CC2420Radio.createMedium(synchronizer, lossyModel);
            else
                return cc2420_medium = CC2420Radio.createMedium(synchronizer, radiusModel);
        }

        private Medium createCC1000Medium() 
        {
            if (cc1000_medium != null) 
                return cc1000_medium;
            
            createRadioModel();
            
            if ( LOSSY_MODEL.get() )
                return cc1000_medium = CC1000Radio.createMedium(synchronizer, lossyModel);
            else
                return cc1000_medium = CC1000Radio.createMedium(synchronizer, radiusModel);
        }
        
        private void createRadioModel() 
        {
            if (topology == null)
                return;

            if ( LOSSY_MODEL.get() )
                lossyModel = new LossyModel();
            else 
                radiusModel = new RadiusModel( 1.0, RANGE.get() );
        }


        private void setNodePosition() 
        {
            topology.addNode(this);
            Topology.Position pos = topology.getPosition(id);
            
            if (pos == null || radio == null)
                return;
            
            if ( LOSSY_MODEL.get() ) 
                lossyModel.setPosition(radio, pos);
            else
                radiusModel.setPosition(radio, pos);
        }

        private void updateNodeID() 
        {
            if ( !UPDATE_NODE_ID.get() )
                return;

            for ( Simulator sim : simulators )
            {    
                SourceMapping smap = sim.getProgram().getSourceMapping();

                if ( smap == null ) 
                    continue;

                updateVariable(smap, "TOS_LOCAL_ADDRESS",           id, sim ); // TinyOS 1.1
                updateVariable(smap, "node_address",                id, sim ); // SOS
                updateVariable(smap, "TOS_NODE_ID",                 id, sim ); // Tinyos 2.0
                updateVariable(smap, "ActiveMessageAddressC$addr",  id, sim ); // Tinyos 2.0
                updateVariable(smap, "ActiveMessageAddressC__addr", id, sim ); // Tinyos 2.1.1 with nesC 1.3.1
            }
        }

        private void updateVariable(SourceMapping   smap, 
                                    String          name, 
                                    int             value,
                                    Simulator       sim) 
        {
            SourceMapping.Location location = smap.getLocation(name);
            
            if ( location == null )
                return;
            
            AtmelInterpreter bi = (AtmelInterpreter) sim.getInterpreter();
            bi.writeFlashByte( location.lma_addr,   Arithmetic.low(value) );
            bi.writeFlashByte( location.lma_addr+1, Arithmetic.high(value) );
        }

        /**
         * The <code>remove()</code> method removes this node from the simulation. This method extends the
         * default simulation remove method by removing the node from the radio air implementation.
         */
        protected void remove() {
            super.remove();
            
            //Every simulator was signaled to stop in the node implementation, which will signal the threads to stop
            //Thread responsible for the radio communication has been removed from the main (radio) synchronizer
            //Reference to node has been removed from simulation, thus the instance should get deleted by the garbage collector
            //There's nothing else to do here, in fact do we really need this method?
        }
    }

    Topology topology;
    noise noise;
    LossyModel lossyModel;
    RadiusModel radiusModel;
    Medium cc2420_medium;
    Medium cc1000_medium;
    long stagger;

    public Multi_Core_Sensor_Simulation() {
        super("sensor-network", HELP, null);
        addSection("SENSOR NETWORK SIMULATION OVERVIEW", help);
        addOptionSection("This simulation type supports simulating multiple sensor network nodes that communicate " +
                "with each other over radios. There are options to specify how many of each type of sensor node to " +
                "instantiate, as well as the program to be loaded onto each node, and an optional topology file " +
                "that describes the physical layout of the sensor network. Also, each node's sensors can be " +
                "supplied with random or replay sensor data through the \"sensor-data\" option. " +
                "For sensor-network simulation type, the default platform is set to \"micaz\" and "+
                "the default monitors are set to \"leds,packet\".", options);

        PLATFORM.setNewDefault("micaz_multicore");  // set the new default monitors
        MONITORS.setNewDefault("leds,packet"); // set the new default monitors
    }

    protected PlatformFactory[] getPlatformFactory() 
    {
        PlatformFactory[] pf_fact_list;
        
        if ( !PLATFORM.isBlank() )
        {
            String delims = "[,]+"; // Delimiter characters, currently only ","
            String[] tokens = PLATFORM.get().split(delims);

            pf_fact_list = new PlatformFactory[tokens.length];
            
            for (int i = 0; i < tokens.length; i++)
                pf_fact_list[i] = Defaults.getPlatform( tokens[i] );
            
            return pf_fact_list;
        }
        
        pf_fact_list = new PlatformFactory[1];
       
        long hz = CLOCKSPEED.get();
        long exthz = EXTCLOCKSPEED.get();
        if (exthz == 0)
            exthz = hz;
        if (exthz > hz)
            Util.userError("External clock is greater than main clock speed", exthz + "hz");
        
        pf_fact_list[0] = new MicaZ_MultiCore.Factory();
        
        return pf_fact_list;
    }
        
        
    /**
     * The <code>process()</code> method processes options and arguments from the command line. In this
     * implementation, this method accepts multiple programs from the command line as arguments as well
     * as options that describe how many of each type of node to instantiate.
     * @param o the options from the command line
     * @param args the arguments from the command line
     * @throws Exception if there is a problem loading any of the files or instantiating the simulation
     */
    public void process(Options o, String[] args) throws Exception 
    {
        options.process(o);
        processMonitorList();

        if ( args.length == 0 )
            Util.userError("Simulation error", "No program specified");
        
        Main.checkFilesExist(args);

        // build the synchronizer
        synchronizer = new RippleSynchronizer(100000, null);

        // create the topology
        processTopology();
        
        // create the nodes based on arguments
        Create_All_Node_Skeletons( args, getPlatformFactory() );

        // process the sensor data input option
        processSensorInput();

        //create Noise time trace
        createNoise();
    }

    private void Create_All_Node_Skeletons(String[] args, PlatformFactory[] pf_fact_list) throws Exception 
    {
        Iterator<String>    node_count_iter = NODECOUNT.get().iterator();
        int                 arg_cnt, pf_fact_cnt; 
        
        for ( arg_cnt = 0, pf_fact_cnt = 0; arg_cnt < args.length; arg_cnt++, pf_fact_cnt++ ) 
        {
            if ( pf_fact_cnt >= pf_fact_list.length )
                pf_fact_cnt = pf_fact_list.length - 1;
            
            
            String delims = "[,]+"; // Delimiter characters, currently only ","
            String[] tokens = args[arg_cnt].split(delims);

            LoadableProgram[] loadprog_list = new LoadableProgram[tokens.length];
            
            for (int i = 0; i < tokens.length; i++)
            {
                loadprog_list[i] = new LoadableProgram( tokens[i] );
                loadprog_list[i].load();
            }
                    
            // create a number of nodes with the same programs
            int count = node_count_iter.hasNext() ? StringUtil.evaluateIntegerLiteral( (String)node_count_iter.next() ) : 1;
            
            for ( int node_cnt = 0; node_cnt < count; node_cnt++ ) 
            {
                Multi_Core_Sensor_Node node = (Multi_Core_Sensor_Node) Create_Node_Skeleton( pf_fact_list[pf_fact_cnt], loadprog_list );
                
                node.startups = new long[ pf_fact_list[pf_fact_cnt].Get_Number_Of_Cores() ];
                
                for (int i = 0; i < node.startups.length; i++)
                    node.startups[i] = processRandom() + processStagger();
            }
        }
    }
    
    public synchronized Node Create_Node_Skeleton(PlatformFactory pf, LoadableProgram[] loadprogs) 
    {
        if (running)
            return null;
        
        int id = num_nodes++;
        Node n = new Multi_Core_Sensor_Node(id, pf, loadprogs);
        
        if (id >= nodes.length)
        {
            Node[] nnodes = new Node[nodes.length * 2];
            System.arraycopy(nodes, 0, nnodes, 0, nodes.length);
            nodes = nnodes;            
        }
        
        nodes[id] = n;
        return n;
    }    

    @Override
    public synchronized void start() {
        // if we are already running, do nothing
        if (running)
            return;

        instantiateNodes();
        
        for (Node node : nodes)
        {
            if ( node == null )
                continue;
            
            for ( SimulatorThread thread : node.getThreads() )
                if ( !thread.isAlive() )
                    thread.start();
        }
        
        running = true;
    }    
    
    @Override    
    public synchronized void join() throws InterruptedException 
    {
        for (Node node : nodes)
        {
            if ( node == null )
                continue;
            
            for ( SimulatorThread thread : node.getThreads() )
                if ( !thread.isAlive() )
                    thread.join();
        }
        
        synchronizer.join();
    }    
    
    private void createNoise() throws Exception 
    {
        if (noise != null)
            return;
        
        noise = NOISE.isBlank() ? new noise() : new noise( NOISE.get() );
    }

    private void processSensorInput() 
    {
       for ( String str : SENSOR_DATA.get() ) 
       {
            int ind = str.indexOf(':');
            
            if ( ind <= 0 )
                Util.userError("Sensor data format error", str);
            
            String sensor = str.substring(0, ind);
            String rest = str.substring(ind+1);
            int ind2 = rest.indexOf(':');
            
            if ( ind2 <= 0 )
                Util.userError("Sensor data format error", str);
            
            String id = rest.substring(0, ind2);
            String file = rest.substring(ind2+1);

            addSensorData(id, file, sensor);
        }
    }

    private void addSensorData(String id, String file, String sensor) 
    {
        int num = StringUtil.evaluateIntegerLiteral(id);
        Multi_Core_Sensor_Node node = (Multi_Core_Sensor_Node)getNode(num);
        
        if ( node == null )
            return;

        SensorDataInput sdi = new SensorDataInput();
        sdi.fname = file;
        sdi.sensor = sensor;
        node.sensorInput.add(sdi);
        
        if ( ! ".".equals(file) )
            Main.checkFileExists(file);
    }

    long processRandom() 
    {
        long low = RANDOM_START.getLow();
        long size = RANDOM_START.getHigh() - low;
        long delay = 0;
        
        if (size > 0) 
        {
            Random r = getRandom();
            delay = r.nextLong();
            if (delay < 0) delay = -delay;
            delay = delay % size;
        }

        return (low + delay);
    }

    long processStagger() {
        //TODO: IS THIS RIGHT???
        long st = stagger;
        stagger += STAGGER_START.get();
        return st;
    }

    private void processTopology() 
    {
        if ( TOPOLOGY.isBlank() )
            return;
       
        topology = Defaults.getTopology( TOPOLOGY.get() );
        // Util.userError("No such topology model", TOPOLOGY.get());
        topology.processOptions(options);
        topology.start();
    }
}
