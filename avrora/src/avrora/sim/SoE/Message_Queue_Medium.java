/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package avrora.sim.SoE;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.HashMap;

import avrora.sim.clock.Clock;
import avrora.sim.clock.Synchronizer;
import avrora.sim.Simulator;
import avrora.sim.util.TransactionalList;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map;

import static java.lang.Math.*;


/**
 * The <code>Medium</code> definition drives the timming in the transmission and
 * reception of packets
 *
 * @author Ben L. Titzer
 * @author Rodolfo de Paz
 */

public class Message_Queue_Medium 
{
    /**
     * The <code>Probe</code> interface defined method to insert and remove
     * probes before and after transmit and receive.
     */
    public interface Msg_Queue_TX_Probe 
    {
        public void Fire_Before_Buffer_Request_Transmit( byte TX_ID_on_core );
        
        public void Fire_After_Buffer_Response_Receive( byte TX_ID_on_core, boolean buffer_granted );

        public void Fire_Before_Transmit( byte TX_ID_on_core, byte val );
        public void Fire_Before_Transmit_End( byte TX_ID_on_core  );
        
        public class Empty implements Msg_Queue_TX_Probe 
        {
            @Override
            public void Fire_Before_Buffer_Request_Transmit( byte TX_ID_on_core )
            {}
        
            @Override
            public void Fire_After_Buffer_Response_Receive( byte TX_ID_on_core, boolean buffer_granted )
            {}
        
            @Override
            public void Fire_Before_Transmit( byte TX_ID_on_core, byte val )
            {}
            
            @Override
            public void Fire_Before_Transmit_End( byte TX_ID_on_core )
            {}
        }

        /**
         * The <code>ProbedTransList</code> class inherits from TransactionalList several
         * methods to implement all methods of the interface Probe
         */
        public class Msg_Queue_TX_Probe_Trans_List extends TransactionalList implements Msg_Queue_TX_Probe 
        {
            @Override
            public void Fire_Before_Buffer_Request_Transmit( byte TX_ID_on_core )
            {
                beginTransaction();
                for (Link pos = head; pos != null; pos = pos.next)
                    ((Msg_Queue_TX_Probe) pos.object).Fire_Before_Buffer_Request_Transmit( TX_ID_on_core );
                endTransaction();
            }

            @Override
            public void Fire_After_Buffer_Response_Receive(  byte TX_ID_on_core, boolean buffer_granted )
            {
                beginTransaction();
                for (Link pos = head; pos != null; pos = pos.next)
                    ((Msg_Queue_TX_Probe) pos.object).Fire_After_Buffer_Response_Receive( TX_ID_on_core, buffer_granted );
                endTransaction();
            }
            
            @Override
            public void Fire_Before_Transmit( byte TX_ID_on_core, byte val)
            {
                beginTransaction();
                for (Link pos = head; pos != null; pos = pos.next)
                    ((Msg_Queue_TX_Probe) pos.object).Fire_Before_Transmit(TX_ID_on_core, val);
                endTransaction();
            }
            
            @Override
            public void Fire_Before_Transmit_End( byte TX_ID_on_core )            
            {
                beginTransaction();
                for (Link pos = head; pos != null; pos = pos.next)
                    ((Msg_Queue_TX_Probe) pos.object).Fire_Before_Transmit_End( TX_ID_on_core );
                endTransaction();
            }
        }
    }

    
    public interface Msg_Queue_RX_Probe  
    {
        public void Fire_After_Buffer_Request_Receive( byte TXRX_ID_on_medium );
        
        public void Fire_Before_Buffer_Response_Transmit( byte TXRX_ID_on_medium );

        public void Fire_After_Receive( byte TXRX_ID_on_medium, byte val );
        
        public void Fire_After_Receive_End( byte TXRX_ID_on_medium );


        public class Empty implements Msg_Queue_RX_Probe 
        {
            @Override
            public void Fire_After_Buffer_Request_Receive( byte TXRX_ID_on_medium )
            {}
        
            @Override
            public void Fire_Before_Buffer_Response_Transmit( byte TXRX_ID_on_medium )
            {}

            @Override
            public void Fire_After_Receive( byte TXRX_ID_on_medium, byte val)
            {}
            
            @Override
            public void Fire_After_Receive_End( byte TXRX_ID_on_medium )
            {}
        }

        /**
         * The <code>ProbedTransList</code> class inherits from TransactionalList several
         * methods to implement all methods of the interface Probe
         */
        public class Msg_Queue_RX_Probe_Trans_List extends TransactionalList implements Msg_Queue_RX_Probe 
        {
            @Override
            public void Fire_After_Buffer_Request_Receive( byte TXRX_ID_on_medium )
            {
                beginTransaction();
                for (Link pos = head; pos != null; pos = pos.next)
                    ((Msg_Queue_RX_Probe) pos.object).Fire_After_Buffer_Request_Receive( TXRX_ID_on_medium );
                endTransaction();
            }

            @Override
            public void Fire_Before_Buffer_Response_Transmit( byte TXRX_ID_on_medium )
            {
                beginTransaction();
                for (Link pos = head; pos != null; pos = pos.next)
                    ((Msg_Queue_RX_Probe) pos.object).Fire_Before_Buffer_Response_Transmit( TXRX_ID_on_medium );
                endTransaction();
            }
            
            @Override
            public void Fire_After_Receive( byte TXRX_ID_on_medium, byte val )
            {
                beginTransaction();
                for (Link pos = head; pos != null; pos = pos.next)
                    ((Msg_Queue_RX_Probe) pos.object).Fire_After_Receive(TXRX_ID_on_medium, val);
                endTransaction();
            }
            
            @Override
            public void Fire_After_Receive_End( byte TXRX_ID_on_medium )            
            {
                beginTransaction();
                for (Link pos = head; pos != null; pos = pos.next)
                    ((Msg_Queue_RX_Probe) pos.object).Fire_After_Receive_End( TXRX_ID_on_medium );
                endTransaction();
            }
        }
    }
    
    
    /**
     * The <code>Medium.TXRX</code> static class represents a Medium where
     * transmitter and receiver exchange bytes
     */
    private static abstract class TXRX 
    {
        protected   final Message_Queue_Medium      medium;
        protected   final Clock                     clock;
        public      final ArrayList<Class>          in_msg_types;
        public      final HashMap<Class, Integer>   out_msg_types;
        
        protected   final byte                      TXRX_ID_on_medium;

        
        /**
         * The <code>TXRX</code> constructor method
         *
         * @param m Medium
         * @param c Clock
         */
        protected TXRX( Message_Queue_Medium    medium, 
                        Clock                   clock,
                        ArrayList<Class>        in_msg_types,
                        HashMap<Class, Integer> out_msg_types,
                        byte                    TXRX_ID_on_medium) 
        {
            this.medium             = medium;
            this.clock              = clock;
            this.in_msg_types       = in_msg_types;
            this.out_msg_types      = out_msg_types;
            
            this.TXRX_ID_on_medium  = TXRX_ID_on_medium;
                    
            medium.Register_TXRX(this);
        }
        
        
        protected LinkedList<Msg> Get_Next_Msgs_And_Insert_Event( Simulator.Event ticker )
        {
            int max_time_step = medium.Get_Max_Time_Step( this );
            
            //Get transmissions
            LinkedList<Msg> next_msgs = medium.Get_Earliest_Msgs( this );
                
            if ( next_msgs.isEmpty() ) //There are no transmissions going on for me?
            {
                clock.insertEvent( ticker, max_time_step );
                    
                return null;
            }
                
            //Are transmissions already relevant, or do we have to wait a little longer?
            long delta = next_msgs.get(0).end_time - clock.getCount();
                
            assert ( delta >= 0 );
                
            if ( delta > 0 ) //We have to return to these events or transmissions later
            {
                clock.insertEvent( ticker, min(max_time_step, delta) );
                    
                return null;
            } 
                
            //Note if a transmission is relevant now, it was automatically removed from the list by Get_Earliest_Msgs()
            //Get transmissions after current transmissions, we want to know when those will happen...
            LinkedList<Msg> after_next_msgs = medium.Get_Earliest_Msgs( this );
            
            delta = max_time_step;
            if ( !after_next_msgs.isEmpty() ) //There are transmissions going on after this?
                delta = min( max_time_step, after_next_msgs.get(0).end_time - clock.getCount() );
                
            clock.insertEvent( ticker, delta );
            
            return next_msgs;
        }
    }


    public static abstract class Transmitter extends TXRX 
    {
        public enum Transmitter_States 
        {
            IDLE_STATE,
            REQUEST_BUFFER_SPACE_STATE,
            WAIT_FOR_SPACE_GRANTED_STATE,
            TRANSMISSION_STATE
        }
        
        private     final Ticker                                        ticker  = new Ticker();
        private     Transmitter_States                                  state   = Transmitter_States.IDLE_STATE;
        private     Msg.Session_Params                                  session = null;
        
        protected   Msg_Queue_TX_Probe.Msg_Queue_TX_Probe_Trans_List    probe_list;        
        
        public      final byte                                          TX_ID_on_core;

        
        public abstract byte[]  Get_Next_Data();
        public abstract boolean Is_There_Next_Data();
        
        
        ////////////////////////////////////////////////////////////////////////
        public Transmitter(Message_Queue_Medium     medium, 
                            Clock                   clock,
                            final int               request_buffer_space_min_time_length,
                            final int               transfer_data_bytes_min_time_length,
                            byte                    TX_ID_on_core,
                            byte                    TXRX_ID_on_medium) 
        {
            super( medium, 
                    clock,
                    new ArrayList<Class>(Arrays.asList( Message_Queue_Medium.Msg_Respond_Buffer_Space.class )),
                    new HashMap<Class, Integer>(){{ put(Message_Queue_Medium.Msg_Request_Buffer_Space.class, request_buffer_space_min_time_length); put(Message_Queue_Medium.Msg_Transfer_Data_Bytes.class, transfer_data_bytes_min_time_length);}},
                    TXRX_ID_on_medium);
            
            this.TX_ID_on_core  = TX_ID_on_core;
        }
        
        
        ////////////////////////////////////////////////////////////////////////
        public Transmitter(Message_Queue_Medium     medium, 
                            Clock                   clock,
                            byte                    TX_ID_on_core,
                            byte                    TXRX_ID_on_medium) 
        {
            this( medium, 
                    clock,
                    Message_Queue_Medium.Msg_Request_Buffer_Space.DEFAULT_MIN_TIME_LENGTH,
                    Message_Queue_Medium.Msg_Transfer_Data_Bytes.DEFAULT_MIN_TIME_LENGTH,
                    TX_ID_on_core,
                    TXRX_ID_on_medium);
        }        
        
        
        ////////////////////////////////////////////////////////////////////////
        public final void Begin_Transmit_Session() 
        {
            if ( state != Transmitter_States.IDLE_STATE )
                return;
            
            if ( probe_list != null ) 
                probe_list.Fire_Before_Buffer_Request_Transmit( TX_ID_on_core );
            
            Msg msg = new Msg_Request_Buffer_Space( this, medium.receiver, clock.getCount() );
            medium.Inject_Msg(this, msg);
           
            session = msg.session;
            state   = Transmitter_States.REQUEST_BUFFER_SPACE_STATE;
            
            clock.insertEvent( ticker, msg.time_length );        
        }


        ////////////////////////////////////////////////////////////////////////   
        public final void End_Transmit_Session() 
        {
            clock.removeEvent(ticker);
            
            session = null;
            state = Transmitter_States.IDLE_STATE;
        }


        ////////////////////////////////////////////////////////////////////////
        protected class Ticker implements Simulator.Event
        {
            public void fire() 
            {
                switch ( state ) 
                {
                    case REQUEST_BUFFER_SPACE_STATE: 
                    {
                        //Buffer space requested, now it's up to the receiver
                        state = Transmitter_States.WAIT_FOR_SPACE_GRANTED_STATE;

                        int max_time_step = medium.Get_Max_Time_Step( Transmitter.this );
                        clock.insertEvent( ticker, max_time_step ); 
                        
                        break;
                    }
                        
                    case WAIT_FOR_SPACE_GRANTED_STATE: //if there's empty space in the receiver buffer we start copying data
                    {
                        //Wait for others (or for the receiver to be precise) to catch up to this thread
                        medium.Wait_For_Neighbors( clock.getCount() );
                        
                        //Get transmission(s) meant for this transmitter
                        LinkedList<Msg> next_msgs = Get_Next_Msgs_And_Insert_Event( this );
                        
                        if ( next_msgs == null) //Nothing to do at this point
                            break;

                        //Note that there may be more transmissions meant for this point in time, in which case we only take the first
                        //The rest is left untouched, and thus gets eventually removed 
                        Msg_Respond_Buffer_Space earliest_msg = (Msg_Respond_Buffer_Space) next_msgs.getFirst();

                        if ( probe_list != null ) 
                            probe_list.Fire_After_Buffer_Response_Receive( TX_ID_on_core, earliest_msg.granted );                        
                        
                        //Was there sufficient empty space in the receiver buffer?                        
                        if ( !earliest_msg.granted ) //This was set by the receiver thread while we were waiting
                        {
                            //No space left, session terminated
                            
                            //TODO: callback to device maybe?
                            
                            session = null;
                            state   = Transmitter_States.IDLE_STATE; 
                    
                            break;                            
                        }
                        
                        //Yay! Rejoice, for we have been granted space!
                        state = Transmitter_States.TRANSMISSION_STATE; 
                        
                        //Notice how there is no break here!!!!!                         
                    }
                        
                    case TRANSMISSION_STATE: // transmit a single byte and add it to the buffer
                    {
                        byte[] data = Get_Next_Data();
                        
                        if ( data == null || data.length == 0 ) //No more data to transmit
                        {
                            if ( probe_list != null ) 
                                probe_list.Fire_Before_Transmit_End( TX_ID_on_core );
                            
                            state = Transmitter_States.IDLE_STATE; 
                    
                            break;                            
                        }

                        session.over = !Is_There_Next_Data(); //No more data to transmit
                        
                        
                        Msg msg = new Msg_Transfer_Data_Bytes( Transmitter.this, 
                                                                medium.receiver, 
                                                                clock.getCount(), 
//                                                                Msg_Transfer_Data_Bytes.DEFAULT_TIME_LENGTH, //Transmission time independent of transmitted data length
                                                                session,
                                                                data );
                        
                        medium.Inject_Msg(Transmitter.this, msg);
                    
                        if ( probe_list != null )
                            for (byte val : data)
                                probe_list.Fire_Before_Transmit( TX_ID_on_core, val );
                    
                        clock.insertEvent( ticker, msg.time_length );
                    
                        break;
                    }
                }
            }
        }
        
        public void insertProbe(Message_Queue_Medium.Msg_Queue_TX_Probe probe) 
        {
            if ( this.probe_list == null ) 
                this.probe_list = new Msg_Queue_TX_Probe.Msg_Queue_TX_Probe_Trans_List();
            
            this.probe_list.add(probe);
        }

        public void removeProbe(Message_Queue_Medium.Msg_Queue_TX_Probe probe) 
        {
            if ( this.probe_list != null ) 
                this.probe_list.remove(probe);
        }        
        
    }

    /**
     * The <code>Medium.Receiver</code> class represents an object that can receive transmissions
     * from the medium. When activated, it listens for transmissions synchronously using
     * its own clock-level synchronization. It receives transmissions that may be the
     * result of multiple interfering transmissions.
     */
    public static abstract class Receiver extends TXRX 
    {
        private     final Ticker                                        ticker          = new Ticker();
        private     HashMap<Msg.Session_Params, Msg_Entry>              msg_entry_map   = new HashMap<>(); //Shows which session is working with which message entry
        
        protected   Msg_Queue_RX_Probe.Msg_Queue_RX_Probe_Trans_List    probe_list;
    
        public abstract Msg_Entry  Get_Msg_Entry();
        
        ////////////////////////////////////////////////////////////////////////
        public static class Msg_Entry
        {
            public  final byte[]        buffer;
            public  int                 buffer_cnt  = 0;
            public  Msg_Entry_States    msg_entry_state   = Msg_Entry_States.EMPTY_STATE;
            public  byte                TXRX_ID_on_medium;

            public enum Msg_Entry_States 
            {
                EMPTY_STATE,
                FILLING_UP_STATE,
                DONE_STATE
            }        
        
            public Msg_Entry(byte[] buffer)
            {
                this.buffer = buffer;
            }
        }           
        
        
        ////////////////////////////////////////////////////////////////////////
        public Receiver(Message_Queue_Medium    medium, 
                            Clock               clock,
                            final int           respond_buffer_space_min_time_length,
                            byte                TXRX_ID_on_medium) 
        {
            super( medium, 
                    clock,
                    new ArrayList<Class>(Arrays.asList( Message_Queue_Medium.Msg_Request_Buffer_Space.class, Message_Queue_Medium.Msg_Transfer_Data_Bytes.class )),
                    new HashMap<Class, Integer>(){{ put(Message_Queue_Medium.Msg_Respond_Buffer_Space.class, respond_buffer_space_min_time_length); }},
                    TXRX_ID_on_medium);
            
            Begin_Receive_Sessions();
        }
        
        
        ////////////////////////////////////////////////////////////////////////
        public Receiver(Message_Queue_Medium    medium, 
                            Clock               clock,
                            byte                TXRX_ID_on_medium) 
        {
            this( medium, 
                    clock,
                    Message_Queue_Medium.Msg_Respond_Buffer_Space.DEFAULT_MIN_TIME_LENGTH,
                    TXRX_ID_on_medium);
        }         
        
        //Begin receiving. Insert an event.
        public final void Begin_Receive_Sessions() 
        {
            msg_entry_map.clear();
            clock.insertEvent( ticker, 1 ); //One clock cycle from now start listening
        }

        //Ending reception. Remove event.
        public final void End_Receive_Sessions() 
        {
            clock.removeEvent(ticker);
            
            for ( Msg_Entry msg_entry : msg_entry_map.values() )
                msg_entry.msg_entry_state = Msg_Entry.Msg_Entry_States.DONE_STATE;
                
            msg_entry_map.clear();
        }

        /**
         * The <code>Ticker</code> class implements a Simulator Event call Ticker
         * that is fired  when a timed event occurs within the simulator in order
         * to model a mote reception
         */
        protected class Ticker implements Simulator.Event 
        {
            ////////////////////////////////////////////////////////////////////
            public void fire() 
            {
                //////////////
                // 1. Figure when to insert next event
                
                //Wait until all neighbors are in time before a possible tx to this thread
                medium.Wait_For_Neighbors( clock.getCount() );
                
                //Get transmission(s) meant for this transmitter
                LinkedList<Msg> next_msgs = Get_Next_Msgs_And_Insert_Event( this );
                        
                if ( next_msgs == null) //Nothing to do at this point
                    return;
                
                
                //////////////
                // 2. Actually take care of current events
                
                //These events or transmissions have to be dealt with right now
                for ( Msg t: next_msgs )
                {
                    if ( t instanceof Msg_Request_Buffer_Space ) //Transmitter wants to know if there is empty space in buffer
                    {
                        if (probe_list != null)
                            probe_list.Fire_After_Buffer_Request_Receive( t.source.TXRX_ID_on_medium );
                        
                        Msg_Entry msg_entry = Get_Msg_Entry();
                        
                        boolean granted = ( msg_entry != null );
                        
                        Msg msg = new Msg_Respond_Buffer_Space( Receiver.this, 
                                                                t.source, 
                                                                clock.getCount(), 
                                                                t.session, 
                                                                granted );
                        
                        msg.session.over = !granted;
                        
                        if (probe_list != null)
                            probe_list.Fire_Before_Buffer_Response_Transmit( t.source.TXRX_ID_on_medium );
                        
                        medium.Inject_Msg(Receiver.this, msg);
                        
                        if (!granted)
                            continue;

                        msg_entry.msg_entry_state   = Msg_Entry.Msg_Entry_States.FILLING_UP_STATE;
                        msg_entry_map.put( t.session, msg_entry );
                    }
                            
                    if ( t instanceof Msg_Transfer_Data_Bytes ) //Transmitter is sending us precious data
                    {
                        assert ( msg_entry_map.containsKey(t.session) );
                        
                        if ( !msg_entry_map.containsKey(t.session) )
                            continue;
                        
                        Msg_Entry msg_entry = msg_entry_map.get( t.session );

                        byte[] data = ((Msg_Transfer_Data_Bytes) t).data;
                        
                        int copy_cnt = (int) min( data.length, msg_entry.buffer.length - msg_entry.buffer_cnt );
                        System.arraycopy( data, 
                                            0, 
                                            msg_entry.buffer, 
                                            msg_entry.buffer_cnt, 
                                            copy_cnt );
                        
                        msg_entry.buffer_cnt += copy_cnt;
                        
                        if (probe_list != null)
                            for ( int i = 0; i < copy_cnt; i++ )
                                probe_list.Fire_After_Receive( t.source.TXRX_ID_on_medium, (byte) data[i] );
                        
                        if ( !t.session.over )
                            continue;
                        
                        msg_entry.msg_entry_state   = Msg_Entry.Msg_Entry_States.DONE_STATE;
                        msg_entry.TXRX_ID_on_medium = t.source.TXRX_ID_on_medium;
                        
                        //TODO: right now buffer is marked done only if "session over" is indicated in the transmission
                        //We should have a time out though...
                        
                        msg_entry_map.remove(t.session);
                            
                        if (probe_list != null) 
                            probe_list.Fire_After_Receive_End( t.source.TXRX_ID_on_medium );
                    }
                }
            }
        }
        
        public void insertProbe(Message_Queue_Medium.Msg_Queue_RX_Probe probe) 
        {
            if ( this.probe_list == null ) 
                this.probe_list = new Msg_Queue_RX_Probe.Msg_Queue_RX_Probe_Trans_List();
            
            this.probe_list.add(probe);
        }

        public void removeProbe(Message_Queue_Medium.Msg_Queue_TX_Probe probe) 
        {
            if ( this.probe_list != null ) 
                this.probe_list.remove(probe);
        }        
        
    }
    
    
    ////////////////////////////////////////////////////////////////////////////
    //
    //
    //
    ////////////////////////////////////////////////////////////////////////////
    public static abstract class Msg 
    {
        public  final TXRX  source;
        public  final TXRX  destination;
        
        public  final long  start_time;
        public  final long  end_time;
        public  final long  time_length;
        
        public  final Session_Params  session;
        
        
        ////////////////////////////////////////////////////////////////////////
        public static class Session_Params
        {
            public boolean  over;
        
            public Session_Params()
            {
                over = false;
            } 
        }

        ////////////////////////////////////////////////////////////////////////
        public Msg(TXRX             source,
                    TXRX            destination,
                    long            start_time,
                    long            time_length,
                    Session_Params  session) //in cycles
        {
            this.source         = source;
            this.destination    = destination;
        
            this.start_time     = start_time;
            this.time_length    = time_length;
            this.end_time       = start_time + time_length;
            
            this.session        = session;
        }
    }
    
    
    ////////////////////////////////////////////////////////////////////////////
    //
    //
    //
    ////////////////////////////////////////////////////////////////////////////
    public static class Msg_Request_Buffer_Space extends Msg
    {
        public  final static int    DEFAULT_MIN_TIME_LENGTH = 250;
        
        ////////////////////////////////////////////////////////////////////////
        public Msg_Request_Buffer_Space(TXRX            source,
                                        TXRX            destination,
                                        long            start_time,
                                        long            time_length,
                                        Session_Params  session) //in cycles
        {
            super(source, 
                destination, 
                start_time, 
                time_length,         
                session);
        }
        
        ////////////////////////////////////////////////////////////////////////
        public Msg_Request_Buffer_Space(TXRX    source,
                                        TXRX    destination,
                                        long    start_time) //in cycles
        {
            super(source, 
                destination, 
                start_time, 
                DEFAULT_MIN_TIME_LENGTH,         
                new Session_Params() );
        }        
    }
    
    
    ////////////////////////////////////////////////////////////////////////////
    //
    //
    //
    ////////////////////////////////////////////////////////////////////////////  
    public static class Msg_Respond_Buffer_Space extends Msg
    {
        public  final static int    DEFAULT_MIN_TIME_LENGTH = 250;        
        public  final boolean       granted;
        
        ////////////////////////////////////////////////////////////////////////
        public Msg_Respond_Buffer_Space(TXRX            source,
                                        TXRX            destination,
                                        long            start_time,
                                        long            time_length,
                                        Session_Params  session,
                                        boolean         granted) //in cycles
        {
            super(source, 
                destination, 
                start_time, 
                time_length, 
                session);
            
            this.granted = granted;
        }        
        
        ////////////////////////////////////////////////////////////////////////
        public Msg_Respond_Buffer_Space(TXRX            source,
                                        TXRX            destination,
                                        long            start_time,
                                        Session_Params  session,
                                        boolean         granted) //in cycles
        {
            this(source, 
                destination, 
                start_time, 
                DEFAULT_MIN_TIME_LENGTH, 
                session,
                granted);
        }         
    } 
    
    
    ////////////////////////////////////////////////////////////////////////////
    //
    //
    //
    ////////////////////////////////////////////////////////////////////////////
    public static class Msg_Transfer_Data_Bytes extends Msg
    {
        public  final static int    DEFAULT_MIN_TIME_LENGTH = 1000;
        public  byte[]              data;
        
        ////////////////////////////////////////////////////////////////////////
        public Msg_Transfer_Data_Bytes(TXRX             source,
                                        TXRX            destination,
                                        long            start_time,
                                        long            time_length, //in cycles
                                        Session_Params  session,
                                        byte[]          data) 
        {
            super(source, 
                destination, 
                start_time, 
                time_length, 
                session);
            
            this.data = Arrays.copyOf(data, data.length);
        }  
        
        ////////////////////////////////////////////////////////////////////////
        public Msg_Transfer_Data_Bytes(TXRX             source,
                                        TXRX            destination,
                                        long            start_time,
                                        Session_Params  session,
                                        byte[]          data) 
        {
            this(source, 
                destination, 
                start_time, 
                DEFAULT_MIN_TIME_LENGTH, 
                session,
                data);
        }        
    }
    
    

    public  final Synchronizer      synch;

    public  Receiver                receiver; //the single receiver of this medium!

    private LinkedList<Msg>         msgs            = new LinkedList<>();
    private HashMap<TXRX, Integer>  transceivers    = new HashMap<>(); //contains the maximum wait time based on the msgs of others

    
    /**
     * The constructor for the <code>Medium</code> class creates a new shared transmission
     * medium with the specified properties, including the bits per second, the lead time
     * before beginning transmission, and the minimum transmission size in bits. These
     * parameters are used to configure the medium and to ensure maximum possible simulation
     * performance.
     *
     * @param synch the synchronizer used to synchronize concurrent senders and receivers
     * @param arb   the arbitrator that determines how to merge received transmissions
     * @param bps   the bits per second throughput of this medium
     * @param ltb   the lead time in bits before beginning a transmission and the first bit
     * @param mintl the minimum transmission length
     * @param maxtl the maximum transmission length
     */
    public Message_Queue_Medium( Synchronizer synch ) 
    {
        this.synch  = synch;
    }
 
    public void Wait_For_Neighbors( long time )
    {
        if (synch != null) 
            synch.waitForNeighbors( time );
    }
    
    public int Calculate_Max_Time_Step( TXRX transceiver )
    {
        //calculate max time for every transceiver based on the transmission times
        int max_time_step = Integer.MAX_VALUE;
        
        for ( Class c : transceiver.in_msg_types )
            for ( TXRX t : transceivers.keySet() )
            {
                if ( t == transceiver )
                    continue;
                
                Integer o = t.out_msg_types.get( c );
            
                if ( o == null )
                    continue;
                
                max_time_step = min( max_time_step, o.intValue() );
            }
        
        return max_time_step;    
    }
    
    public byte Register_TXRX( TXRX transceiver )
    {
        transceivers.put(transceiver, 0);
                 
        //calculate max time for every transceiver based on the transmission times
        for ( Map.Entry pairs : transceivers.entrySet() ) 
            pairs.setValue( Calculate_Max_Time_Step( (TXRX) pairs.getKey() ) );

        if ( transceiver instanceof Receiver )
        {    
            receiver = (Receiver) transceiver; 
            return 0;
        }
        
        //this is the ID of the transmitter on the medium
        //Note how the single receiver of the medium always has ID = 0
        
        //Preparing number for byte-integer conversion
        int ID = transceivers.size() - ( receiver == null ? 0 : 1 ); 
        if ( ID > 127 )
            ID -= 256;
        
        return (byte) ID; 
    }
    
    public int Get_Max_Time_Step( TXRX transceiver )
    {
        Integer o = transceivers.get( transceiver );
            
        if ( o == null )
            return 0;
                
        return o.intValue();
    }
        
    public void Inject_Msg( TXRX transceiver, Msg msg )
    {
        Integer o = transceiver.out_msg_types.get( msg.getClass() );
        
        assert o != null;
        
        if ( o == null )
            return;
        
        int min_time_length = o.intValue();
        
        assert msg.time_length >= min_time_length;

        if ( msg.time_length < min_time_length )
            return;

        synchronized ( this ) 
        {
            msgs.add(msg);
        }
    }
    

    
    ////////////////////////////////////////////////////////////////////////
    /*
    private void Clean_Out_Transmissions( TXRX transceiver ) 
    {
        synchronized ( this ) 
        {
            Iterator<Msg> i = msgs.iterator();
                
            while ( i.hasNext() ) 
            {
                Msg msg_entry = i.next();
                    
                if ( msg_entry.destination != transceiver )
                    continue;
                    
                //it's not time for this particular transmission
                if ( msg_entry.end_time < transceiver.clock.getCount() )
                    i.remove(); // remove older transmissions
            }
        }
    }
    */ 
    
    ////////////////////////////////////////////////////////////////////////
    private LinkedList<Msg> Get_Earliest_Msgs( TXRX transceiver ) 
    {
        LinkedList<Msg> next_msgs = new LinkedList<>();
            
        synchronized ( this ) 
        {
            Iterator<Msg> i = msgs.iterator();
                
            while ( i.hasNext() ) 
            {
                Msg msg = i.next();
                    
                if ( msg.destination != transceiver )
                    continue;

                //it's not time for this particular transmission
                if ( msg.end_time <= transceiver.clock.getCount() )
                {
                    i.remove(); // remove older transmissions
                        
                    if ( msg.end_time < transceiver.clock.getCount() )
                        continue;
                }                    
                
                if ( !transceiver.in_msg_types.contains( msg.getClass() ) )
                    continue;

                
                if ( !next_msgs.isEmpty()   &&  msg.end_time < next_msgs.get(0).end_time )
                    next_msgs.clear();
                
                if ( next_msgs.isEmpty()    ||  msg.end_time == next_msgs.get(0).end_time )
                    next_msgs.add(msg);
            }
        }
            
        return next_msgs;
    }
    
}
