/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package avrora.sim.SoE;

import avrora.sim.clock.ClockDomain;
import avrora.sim.radio.Medium;

/**
 *
 * @author babjak
 */
public abstract class System_Of_Elements_With_Mediums extends System_Of_Elements
{
    public  Medium[]    mediums;
        
    protected System_Of_Elements_With_Mediums(ClockDomain[]   clock_domains, 
                                                    int             number_of_pins,
                                                    Medium[]        mediums) 
    {
        super( clock_domains, number_of_pins );
        this.mediums = mediums;
    }
}
