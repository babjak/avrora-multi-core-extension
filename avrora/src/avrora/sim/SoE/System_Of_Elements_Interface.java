package avrora.sim.SoE;

import avrora.sim.clock.Synchronizer;
import avrora.sim.mcu.AtmelInternalDevice;
import avrora.sim.mcu.Microcontroller;

import avrora.sim.platform.Platform;
import java.util.HashMap;

/**
 * The <code>Microcontroller</code> interface corresponds to a hardware device that implements the AVR
 * instruction set. This interface contains methods that get commonly needed information about the particular
 * hardware device and and can load programs onto this virtual device.
 *
 * @author Ben L. Titzer
 */
public interface System_Of_Elements_Interface {

    /**
     * The <code>getSimulator()</code> method gets a simulator instance that is capable of emulating this
     * hardware device.
     *
     * @return a <code>Simulator</code> instance corresponding to this device
     */
//    public LinkedList<Simulator> getSimulators();

    /**
     * The <code>getPlatform()</code> method gets a platform instance that contains this microcontroller.
     *
     * @return the platform instance containing this microcontroller, if it exists; null otherwise
     */
    public Platform getPlatform();

    /**
     * The <code>setPlatform()</code> method sets the platform instance that contains this microcontroller.
     * @param p the new platform for this microcontroller
     */
    public void setPlatform(Platform p);

    /**
     * The <code>getPin()</code> method looks up the named pin and returns a reference to that pin. Names of
     * pins should be UPPERCASE. The intended users of this method are external device implementors which
     * connect their devices to the microcontroller through the pins.
     *
     * @param name the name of the pin; for example "PA0" or "OC1A"
     * @return a reference to the <code>Pin</code> object corresponding to the named pin if it exists; null
     *         otherwise
     */
    public Microcontroller.Pin getPin(String name);

    /**
     * The <code>getPin()</code> method looks up the specified pin by its number and returns a reference to
     * that pin. The intended users of this method are external device implementors which connect their
     * devices to the microcontroller through the pins.
     *
     * @param num the pin number to look up
     * @return a reference to the <code>Pin</code> object corresponding to the named pin if it exists; null
     *         otherwise
     */
    public Microcontroller.Pin getPin(int num);

    /**
     * The <code>sleep()</code> method puts the microcontroller into the sleep mode defined by its
     * internal sleep configuration register. It may shutdown devices and disable some clocks. This
     * method should only be called from within the interpreter.
     */
//    public void sleep();

    /**
     * The <code>wakeup()</code> method wakes the microcontroller from a sleep mode. It may resume
     * devices, turn clocks back on, etc. This method is expected to return the number of cycles that
     * is required for the microcontroller to wake completely from the sleep state it was in.
     *
     * @return cycles required to wake from the current sleep mode
     */
//    public int wakeup();

    /**
     * The <code>getClockDomain()</code> method returns the clock domain for this microcontroller. The clock
     * domain contains all of the clocks attached to the microcontroller and platform, including the main clock.
     * @return an instance of the <code>ClockDomain</code> class representing the clock domain for this
     * microcontroller
     */
//    public ClockDomain getClockDomain();

    /**
     * The <code>getRegisterSet()</code> method returns the register set containing all of the IO registers
     * for this microcontroller.
     * @return a reference to the <code>RegisterSet</code> instance which stores all of the IO registers
     * for this microcontroller.
     */
//    public RegisterSet getRegisterSet();

    /**
     * The <code>getProperties()</code> method gets an object that describes the microcontroller
     * including the size of the RAM, EEPROM, flash, etc.
     * @return an instance of the <code>MicrocontrollerProperties</code> class that contains all
     * the relevant information about this microcontroller
     */
//    public MCUProperties getProperties();
    
    public void setCores( HashMap<String, Microcontroller> cores );
    public HashMap<String, Microcontroller> Get_Cores();
    public int Get_Number_Of_Cores();
    
    public AtmelInternalDevice getDevice(String name);
    
    /*
    public void Synchronizers_Start();
    public void Synchronizers_Pause();    
    public void Synchronizers_Stop();
    public void Synchronizers_Join() throws InterruptedException;
    public Synchronizer[] Get_Synchronizers_For_Core_ID( String core_ID );
    */ 
}
