/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package avrora.sim.SoE;

import avrora.sim.RWRegister;
import avrora.sim.mcu.AtmelMicrocontroller;
import cck.text.StringUtil;

/**
 * Serial Peripheral Interface. Used on the <code>Mica2</code> platform for radio communication.
 *
 * @author Daniel Lee, Simon Han
 */
public class HW_TX_Msg_Queues extends TX_Msg_Queues //implements InterruptTable.Notification 
{
    private final MQDst_Reg        MQDst_reg;
    private final MQS_Reg          MQS_reg;
    private final MQA_Reg          MQA_reg;
    private final MQD_Reg[]        data_reg_array      = new HW_TX_Msg_Queues.MQD_Reg[MSG_SIZE];
    
    public enum Buffer_States 
    {
        IDLE_STATE,
        ALLOCATED_STATE,
        TRANSFERING_STATE
    }
    
    
    public HW_TX_Msg_Queues(AtmelMicrocontroller    mcu, 
                            Message_Queue_Medium[]  mediums,
                            byte                    TXRX_ID_on_medium) 
    {
        super(mcu, mediums, TXRX_ID_on_medium);
        
        MQDst_reg = new HW_TX_Msg_Queues.MQDst_Reg();
        MQS_reg = new HW_TX_Msg_Queues.MQS_Reg();
        MQA_reg = new HW_TX_Msg_Queues.MQA_Reg();
        
        installIOReg( "TX_MSG_STATUS",  MQS_reg );
        installIOReg( "TX_MSG_DEST",    MQDst_reg );
        installIOReg( "TX_MSG_ALLOC",   MQA_reg );
        
        for ( short i = 0; i < MSG_SIZE; i++)
        {
            data_reg_array[i] = new HW_TX_Msg_Queues.MQD_Reg( i );
            
            installIOReg( "TX_MSG_BYTE_" + i, data_reg_array[i] );
        }
    }

    /**
     * Data registers. Several of these get instantiated to cover all the bytes of the memory mapped message queue.
     */
    class MQD_Reg extends RWRegister 
    {
        public  final int   msg_idx;

        public MQD_Reg( int msg_idx )
        {
            this.msg_idx = msg_idx;
        }
                
        @Override        
        public int getValue() 
        {
            return read_data_byte(msg_idx);
        }

        @Override
        public byte read() 
        {
            return read_data_byte(msg_idx);
        }        
        
        @Override
        public void setValue(int val) 
        {
            write( (byte)val );
        }

        @Override
        public void write(byte val) 
        {
            byte old = read_data_byte(msg_idx);
            
            if ( !write_data_byte(val, msg_idx) )
            {
                if ( devicePrinter != null )
                    devicePrinter.println( "HW TX msg data register: tried to write " + StringUtil.toMultirepString(val, 8) );
                
                return;
            }
                
            notify(old, val);      
        }
    }

    
    /**
     * Destination register.
     */
    class MQDst_Reg extends RWRegister 
    {
        @Override
        public void setValue(int val) 
        {
            write( (byte) val );
        }        
        
        @Override
        public void write(byte val) 
        {
            if ( !is_allocated() )
            {
                if ( devicePrinter != null )
                    devicePrinter.println( "HW TX msg queue control register: tried to write " + StringUtil.toMultirepString(val, 8) );
                
                return;
            }
            
            if ( devicePrinter != null )
                devicePrinter.println( "HW TX msg queue control register: wrote " + StringUtil.toMultirepString(val, 8) );
            
            super.write(val);
            
            if ( !write_to_dest(val) )
                if ( devicePrinter != null )
                    devicePrinter.println( "HW TX msg queue control register: unknown TX device" );
        }
    }

    /**
     * Status register.
     */
    class MQS_Reg extends RWRegister 
    {
        private static final byte   B_BUSY     = 0;
//        private final BooleanView   _B_BUSY    = RegisterUtil.booleanView( this, B_BUSY );        

        private static final byte   B_TRANSFERING   = 1;
//        private final BooleanView   _B_TRANSFERING  = RegisterUtil.booleanView( this, B_TRANSFERING );        

        private void Set_Bit(byte bit_idx, boolean val)
        {
            value = (byte) ( val ? 
                            value | (1 << bit_idx) : 
                            value & ~(1 << bit_idx) );
        }
        
        
        @Override
        public int getValue() 
        {
            return (int) read();
        }
        
        @Override
        public byte read() 
        {
//            _B_BUSY.setValue( buffer_state != Buffer_States.IDLE_STATE );
//            _B_TRANSFERING.setValue( buffer_state == Buffer_States.TRANSFERING_STATE );

            Set_Bit( B_BUSY,        msg_busy() );
            Set_Bit( B_TRANSFERING, msg_transfering()  );
            
            return value;
        }
        
        @Override
        public void setValue(int val) 
        {
            write( (byte) val);
        }
        
        @Override
        public void write(byte val) 
        {
        }
    }

    /**
     * Allocation register.
     */
    class MQA_Reg extends RWRegister 
    {
        private static final byte   B_SUCCESS     = 0;
//        private final BooleanView   _B_SUCCESS    = RegisterUtil.booleanView( this, B_SUCCESS );
        
        private void Set_Bit(byte bit_idx, boolean val)
        {
            value = (byte) ( val ? 
                            value | (1 << bit_idx) : 
                            value & ~(1 << bit_idx) );
        }        
        
        @Override
        public int getValue() 
        {
            return (int) read();
        }
        
        @Override
        public byte read() 
        {
//            _B_SUCCESS.setValue( buffer_state == Buffer_States.IDLE_STATE );
            Set_Bit( B_SUCCESS,   !msg_busy() );

            get_successfull_allocation();
            
            return value;
        }  
        
        @Override
        public void setValue(int val) 
        {
            write( (byte) val);
        }       
        
        @Override
        public void write(byte val) 
        {
            release_allocation();
        }
    }
    
    /*
    @Override
    public void force(int inum) 
    {

    }

    @Override
    public void invoke(int inum)
    {

    }
    */ 
}

