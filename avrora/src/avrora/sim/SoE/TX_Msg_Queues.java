/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package avrora.sim.SoE;

import avrora.sim.mcu.AtmelInternalDevice;
import avrora.sim.mcu.AtmelMicrocontroller;
import avrora.sim.clock.Clock;

/**
 * Serial Peripheral Interface. Used on the <code>Mica2</code> platform for radio communication.
 *
 * @author Daniel Lee, Simon Han
 */
public class TX_Msg_Queues extends AtmelInternalDevice //implements InterruptTable.Notification 
{
    protected final short          MSG_SIZE            = 32; //[Byte]
    
    private final Transmitter[]    transmitters;      
    private byte[]                 buffer              = new byte[MSG_SIZE];
    private Buffer_States          buffer_state        = Buffer_States.IDLE_STATE;
    
    
    
    public enum Buffer_States 
    {
        IDLE_STATE,
        ALLOCATED_STATE,
        TRANSFERING_STATE
    }
    
    
    public class Transmitter extends Message_Queue_Medium.Transmitter 
    {
        public Transmitter(Message_Queue_Medium     medium, 
                            Clock                   clock,
                            byte                    TX_ID_on_core,
                            byte                    TXRX_ID_on_medium) 
        {
            super( medium, clock, TX_ID_on_core, TXRX_ID_on_medium );
        }

        @Override
        public byte[]  Get_Next_Data()
        {
            if ( buffer_state != Buffer_States.TRANSFERING_STATE )
                return null;
            
            //buffer_state = Buffer_States.ALLOCATED_STATE;
            buffer_state = Buffer_States.IDLE_STATE;
            return buffer;
        }
        
        @Override
        public boolean Is_There_Next_Data()
        {
            return (buffer_state == Buffer_States.TRANSFERING_STATE);
        }        
    }
            
    
    public TX_Msg_Queues(AtmelMicrocontroller       mcu, 
                            Message_Queue_Medium[]  mediums,
                            byte                    TXRX_ID_on_medium) 
    {
        super("HWTXMsgQueue", mcu);        
        
        transmitters = new TX_Msg_Queues.Transmitter[ mediums.length ];
        
        for ( short i = 0; i < mediums.length; i++)
            transmitters[i] = new TX_Msg_Queues.Transmitter( mediums[i], 
                                                                mcu.getSimulator().getClock(),
                                                                (byte) (i > 127 ? i - 256 : i),
                                                                TXRX_ID_on_medium);
    }

    public TX_Msg_Queues.Transmitter[] Get_Transmitters()
    {
        return transmitters;    
    }

    
    /**
     * Data 
     */
    public byte read_data_byte(int byte_idx) 
    {
        return buffer[byte_idx];
    }        
        
    public boolean write_data_byte(byte val, int byte_idx) 
    {
        if ( !is_allocated() )
        {
//            if ( devicePrinter != null )
//                devicePrinter.println( "HW TX msg data register: tried to write " + StringUtil.toMultirepString(val, 8) );
                
            return false;
        }
                
        buffer[byte_idx] = val;
        return true;
    }

    
    /**
     * Destination 
     */
    public boolean write_to_dest(byte dest_ID) 
    {
        if ( !is_allocated() )
        {
//            if ( devicePrinter != null )
//                devicePrinter.println( "HW TX msg queue control register: tried to write " + StringUtil.toMultirepString(val, 8) );
                
            return false;
        }
            
//        if ( devicePrinter != null )
//            devicePrinter.println( "HW TX msg queue control register: wrote " + StringUtil.toMultirepString(val, 8) );
            
        int v = (int)  dest_ID;
        if ( v<0 )
            v += 256;
            
        if ( v >= transmitters.length )
        {
//            if ( devicePrinter != null )
//                devicePrinter.println( "HW TX msg queue control register: unknown TX device" );
                
            return false;
        }                
            
        buffer_state = Buffer_States.TRANSFERING_STATE;
            
        transmitters[v].Begin_Transmit_Session();
        
        return true;
    }

    
    /**
     * Status
     */
    public boolean msg_busy() 
    {
        return buffer_state != Buffer_States.IDLE_STATE;
    }

    public boolean msg_transfering() 
    {
        return buffer_state == Buffer_States.TRANSFERING_STATE;
    }    
    
    public boolean is_allocated()
    {
        return buffer_state == Buffer_States.ALLOCATED_STATE;
    }

    
    /**
     * Allocation 
     */    public boolean get_successfull_allocation() 
    {
        boolean val = (buffer_state == Buffer_States.IDLE_STATE);

        if ( val )
            buffer_state = Buffer_States.ALLOCATED_STATE;
            
        return val;
    }  
        
    public void release_allocation() 
    {
        if ( buffer_state == Buffer_States.TRANSFERING_STATE )
            return;
                
        buffer_state = Buffer_States.IDLE_STATE;
    }
}

