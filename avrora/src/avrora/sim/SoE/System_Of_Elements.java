package avrora.sim.SoE;

import avrora.sim.clock.ClockDomain;
import avrora.sim.mcu.AtmelInternalDevice;
import avrora.sim.mcu.Microcontroller;
import avrora.sim.platform.Platform;
import avrora.sim.radio.Medium;
import cck.text.StringUtil;
import java.util.HashMap;
import java.util.NoSuchElementException;

/**
 * @author Ben L. Titzer
 */
public abstract class System_Of_Elements implements System_Of_Elements_Interface 
{
    protected   final Microcontroller.Pin[]             pins;
    protected   HashMap<String, Integer>                pinAssignments;
    protected   Platform                                platform;
//    protected       SimPrinter                          pinPrinter;
    protected   final ClockDomain[]                     clock_domains;
    protected   HashMap<String, Microcontroller>        cores;
    protected   HashMap<String, AtmelInternalDevice>    devices;
    
    
    //TODO: add devices and other System_Of_Elements at some point...
    protected System_Of_Elements(ClockDomain[]  clock_domains, 
                                int             number_of_pins) 
    {
        this.clock_domains = clock_domains;
        pins = new Microcontroller.Pin[number_of_pins];
//        sleepState = sleep;
//        registers = regs;
    }

    /**
     * The <code>getFSM()</code> method gets a reference to the finite state machine that represents
     * the sleep modes of the MCU. The finite state machine allows probing of the sleep mode transitions.
     * @return a reference to the finite state machine representing the sleep mode of the MCU
     */
/*    
    public FiniteStateMachine getFSM() {
        return sleepState;
    }
*/
 
    /**
     * The <code>getRegisterSet()</code> method gets a reference to the register set of the microcontroller.
     * The register set contains all of the IO registers for this microcontroller.
     *
     * @return a reference to the register set of this microcontroller instance
     */
/*    
    public RegisterSet getRegisterSet() {
        return registers;
    }
*/
    
    /**
     * The <code>getPin()</code> method looks up the specified pin by its number and returns a reference to
     * that pin. The intended users of this method are external device implementors which connect their
     * devices to the microcontroller through the pins.
     *
     * @param num the pin number to look up
     * @return a reference to the <code>Pin</code> object corresponding to the named pin if it exists; null
     *         otherwise
     */
    @Override
    public Microcontroller.Pin getPin(int num) 
    {
        if (num < 0 || num > pins.length) 
            return null;
        
        return pins[num];
    }

    @Override
    public Microcontroller.Pin getPin(String n) 
    {
        Integer i = pinAssignments.get(n);
        
        if ( i == null )
            throw new NoSuchElementException(StringUtil.quote(n)+" pin not found");
        
        return getPin( i );
    }    
    
    /**
     * The <code>getClock()</code> method gets a reference to a specific clock on this device. For example,
     * the external clock, or a specific device's clock can be accessed by specifying its name.
     * @param name the name of the clock to get
     * @return a reference to the <code>Clock</code> instance for the specified clock if it exists
     */
    //public Clock getClock(String name) {
    //    return clockDomain.getClock(name);
    //}

    /**
     * The <code>getSimulator()</code> method gets a reference to the simulator for this microcontroller instance.
     * @return a reference to the simulator instance for this microcontroller
     */
/*
    public Simulator[] getSimulators() {
        return simulators;
    }
    */

    /**
     * The <code>getPlatform()</code> method returns the platform for this microcontroller.
     * @return the platform instance containing this microcontroller
     */
    @Override
    public Platform getPlatform() {
        return platform;
    }

    /**
     * The <code>setPlatform()</code> method sets the platform instance for this microcontroller
     * @param p the platform instance associated with this microcontroller
     */
    @Override
    public void setPlatform(Platform p) {
        platform = p;
    }

    @Override
    public void setCores( HashMap<String, Microcontroller> cores )
    {
        this.cores = cores;
    }
    
    @Override
    public  HashMap<String, Microcontroller> Get_Cores()
    {
        return cores;
    }    
    
    
    /**
     * The <code>addDevice()</code> method adds a new internal device to this microcontroller so that it can
     * be retrieved later with <code>getDevice()</code>
     * @param d the device to add to this microcontroller
     */
    
    void addDevice(AtmelInternalDevice d) 
    {
        devices.put(d.name, d);
    }
    
    @Override
    public AtmelInternalDevice getDevice(String name) 
    {
        return devices.get(name);
    }

    /**
     * The <code>getClockDomain()</code> method gets a reference to the <code>ClockDomain</code> instance for
     * this node that contains the main clock and any derived clocks for this microcontroller.
     * @return a reference to the clock domain for this microcontroller
     */
    //public ClockDomain getClockDomain() {
    //    return clockDomain;
    //}
    
    @Override
    public int Get_Number_Of_Cores() 
    {
        return cores.size();
    }
}
