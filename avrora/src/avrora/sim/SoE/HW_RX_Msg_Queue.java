/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package avrora.sim.SoE;

import avrora.sim.RWRegister;
import avrora.sim.mcu.AtmelMicrocontroller;
import avrora.sim.clock.Clock;
import avrora.sim.state.BooleanView;
import avrora.sim.state.RegisterUtil;
import cck.text.StringUtil;
import java.util.LinkedList;

/**
 * Serial Peripheral Interface. Used on the <code>Mica2</code> platform for radio communication.
 *
 * @author Daniel Lee, Simon Han
 */
public class HW_RX_Msg_Queue extends RX_Msg_Queue //implements InterruptTable.Notification
{
    private final MQS_Reg                                               MQS_reg;
    private final MQC_Reg                                               MQC_reg;
    private final MQSrc_Reg                                             MQSrc_reg;
    private final MQD_Reg[]                                             data_reg_array  = new MQD_Reg[MSG_SIZE];
  
    
    public HW_RX_Msg_Queue(AtmelMicrocontroller     mcu, 
                            Message_Queue_Medium    medium,
                            byte                    TXRX_ID_on_medium) 
    {
        super(mcu, medium, TXRX_ID_on_medium);
        
        MQS_reg = new MQS_Reg();
        MQC_reg = new MQC_Reg();
        MQSrc_reg = new MQSrc_Reg();
        
        installIOReg( "RX_MSG_STATUS",  MQS_reg );
        installIOReg( "RX_MSG_CTRL",    MQC_reg );
        installIOReg( "RX_MSG_SOURCE",  MQSrc_reg );

        for ( short i = 0; i < MSG_SIZE; i++)
        {
            data_reg_array[i] = new MQD_Reg( i );
            
            installIOReg( "RX_MSG_BYTE_" + i, data_reg_array[i] );
        }
    }

    /**
     * Data registers
     */
    class MQD_Reg extends RWRegister 
    {
        //public  LinkedList<Message_Queue_Medium.Receiver.Msg_Entry> msg_queue;
        //public  final int                                           queue_idx; 
        public  final int                                           msg_idx;
                                

        public MQD_Reg( //LinkedList<Message_Queue_Medium.Receiver.Msg_Entry> msg_queue, 
                        //int                                                 queue_idx, 
                        int                                                 msg_idx )
        {
            //this.msg_queue  = msg_queue;
            //this.queue_idx  = queue_idx;
            this.msg_idx    = msg_idx;
        }
                
        @Override        
        public int getValue() 
        {
//            return msg_queue.get(queue_idx).buffer[msg_idx];
            return (int) read();
        }

        @Override
        public byte read() 
        {
//            if ( spifAccessed ) 
//                unpostSPIInterrupt();
            
//            return msg_queue.get(queue_idx).buffer[msg_idx];
            return read_data_byte(msg_idx); 
        }        
        
        @Override
        public void setValue(int val) 
        {
            write( (byte)val );
        }

        @Override
        public void write(byte val) 
        {
//            byte old = msg_queue.get(queue_idx).buffer[msg_idx];
//            msg_queue.get(queue_idx).buffer[msg_idx] = val;
            byte old = read_data_byte(msg_idx);
            write_data_byte(val, msg_idx); 
            notify(old, val);        
            //transferEvent.enableTransfer();
        }
    }

    
    /**
     * Msg source register.
     */
    class MQSrc_Reg extends RWRegister 
    {
        public MQSrc_Reg()
        {
        }
                
        @Override        
        public int getValue() 
        {
            int ID = read();
            
            if (ID < 0)
                ID += 256;
            
            return ID;
        }
        
        @Override
        public byte read() 
        {
            return get_msg_ID_byte();
        }  
        
        @Override
        public void setValue(int val) 
        {
            write( (byte) val );
        }

        @Override
        public void write(byte val) 
        {
        }
    }
    
    
    
    /**
     * Control register.
     */
    class MQC_Reg extends RWRegister 
    {
        public  static final int    MQ_MSG_POP  = 0;
        public  final BooleanView   _pop        = RegisterUtil.booleanView(this, MQ_MSG_POP);
        
        public MQC_Reg() 
        {
            value = 0;
        }
        
        
        @Override
        public void setValue(int val) 
        {
            write( (byte) val );
        } 
        
        
        @Override
        public void write(byte val) 
        {
            if ( devicePrinter != null )
                devicePrinter.println( "HW TX msg queue control register: wrote " + StringUtil.toMultirepString(val, 8) );
            
            super.write(val);
            
            if ( !_pop.getValue() )
                return;
            
            pop_first();
                
            _pop.setValue( false );
        }
    }

    /**
     * Status register.
     */
    class MQS_Reg extends RWRegister 
    {
        public  static final byte   MQ_MSG_RDY    = 0;
//        public  final BooleanView   _MQ_MSG_RDY   = RegisterUtil.booleanView(this, MQ_MSG_RDY);

        private void Set_Bit(byte bit_idx, boolean val)
        {
            value = (byte) ( val ? 
                            value | (1 << bit_idx) : 
                            value & ~(1 << bit_idx) );
        }           
        
        @Override
        public int getValue() 
        {
            return (int) read();
        }
        
        @Override
        public byte read() 
        {
//            _MQ_MSG_RDY.setValue( msg_queue.getFirst().msg_entry_state == Message_Queue_Medium.Receiver.Msg_Entry.Msg_Entry_States.DONE_STATE );
            Set_Bit( MQ_MSG_RDY, msg_ready() );
            return value;
        }        
        
        @Override
        public void setValue(int val) 
        {
            write( (byte) val);
        } 
        
        @Override
        public void write(byte val) 
        {
        }
    }
    
    /*
    @Override
    public void force(int inum) 
    {

    }
    
    @Override
    public void invoke(int inum)
    {

    }
    */ 
}

