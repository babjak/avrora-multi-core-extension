package avrora.sim.SoE;

import avrora.core.Program;
import avrora.sim.*;
import avrora.sim.clock.ClockDomain;
import avrora.sim.clock.RippleSynchronizer;
import avrora.sim.clock.Synchronizer;
import avrora.sim.mcu.ATMega128_MC;
import static avrora.sim.mcu.AtmelMicrocontroller.addPin;
import avrora.sim.mcu.IP_Core;
import avrora.sim.mcu.Microcontroller;
import java.util.HashMap;


/**
 * The <code>AtmelMicrocontroller</code> class represents the common functionality among microcontrollers
 * for the Atmel series. These all contain a clock domain (collection of internal, external clocks), a
 * simulator, an interpreter, microcontroller properties, and a mapping between string names and IO reg
 * addresses, etc.
 *
 * @author Ben L. Titzer
 */
public class SoC_MultiCore extends System_Of_Elements {

   // private final MainClock         mainClock; //TODO: is this really needed?
    Synchronizer[]  synchs;

    /*
     * The <code>sleep()</code> method is called by the interpreter when the program executes a SLEEP
     * instruction. This method transitions the microcontroller into a sleep mode, including turning
     * off any devices, shutting down clocks, and transitioning the sleep FSM into a sleep mode.
     *
     * @see Microcontroller#sleep()
     */
/*    
    public void sleep() {
        // transition to the sleep state in the MCUCR register
        sleepState.transition(getSleepMode());
    }
*/

/*    
    protected abstract int getSleepMode();
*/
    /**
     * The <code>wakeup()</code> method is called by the interpreter when the microcontroller is
     * woken from a sleep mode by an interrupt or other event. This method transitions the
     * microcontroller back into active mode, turning back on devices. This method returns
     * the number of clock cycles necessary to wake the MCU from sleep.
     *
     * @return cycles it takes to wake up
     * @see Microcontroller#wakeup()
     */
/*    
    public int wakeup() {
        // transition to the active state (may insert transition event into event queue)
        sleepState.transition(MODE_ACTIVE);
        // return the number of cycles consumed by waking up
        return sleepState.getTransitionTime(sleepState.getCurrentState(), MODE_ACTIVE);
    }
*/
    
    public SoC_MultiCore(int            node_id, 
                        Simulation      sim, 
                        Program[]       progs, 
                        ClockDomain[]   clock_domains,
                        short           number_of_cores,
                        boolean         AE_core_flag,
                        byte            dest_core_ID,
                        byte            dest_comp_ID,
                        byte            dest_interf_ID,
                        byte            dest_func_ID) 
    {
        super( clock_domains, 10 + number_of_cores * 3 );
        
        //mainClock = cd.getMainClock();
       
        //////////////////////
        //mediums for inter core communication
        //For this SoC, each core has its own message queue, thus there are as many
        //mediums as cores. Each core is connected to every other's queue.
        //NOTE THAT THIS IS NOT THE ONLY WAY TO CONNECT CORES!
        //One could come up e.g. with a bus to which all cores would be connected
        //in which case there would be only one medium and synchronizer for the platform.
        assert (number_of_cores == clock_domains.length);
        assert (number_of_cores == synchs.length);
        
        //Three cores -> three input message queue -> three mediums
        Message_Queue_Medium[] mediums = new Message_Queue_Medium[number_of_cores];
        synchs  = new Synchronizer[number_of_cores];
        
        for ( int i = 0; i < number_of_cores; i++ ) 
        {
            synchs[i]   = new RippleSynchronizer( 100000, null );        
            mediums[i]  = new Message_Queue_Medium( synchs[i] ); 
        }
        
        //Create cores 
        cores = new HashMap<>();
            
        for ( int i = 0, prog_cnt = 0; i < number_of_cores; i++, prog_cnt++ )
        {
            if ( prog_cnt >= progs.length )
                prog_cnt = progs.length - 1;
            
            //Last core can be an IP core, so it's treated differently
            if ( i == number_of_cores-1 
                && 
                i != 0 // if only one core is allowed, it can't be an IP core
                &&
                AE_core_flag )
            {
                cores.put(  Integer.toString(i), 
                            new IP_Core(node_id, 
                                        Integer.toString(i),
                                        (byte) (i > 127 ? i - 256 : i),   
                                        sim, 
                                        clock_domains[i], 
                                        progs[prog_cnt], 
                                        mediums[i], 
                                        mediums,
                                        dest_core_ID,
                                        dest_comp_ID,
                                        dest_interf_ID,
                                        dest_func_ID) );
                
                continue;
            }
    
            cores.put(  Integer.toString(i), 
                        new ATMega128_MC(   node_id, 
                                            Integer.toString(i),
                                            (byte) (i > 127 ? i - 256 : i),
                                            sim, 
                                            clock_domains[i], 
                                            progs[prog_cnt], 
                                            mediums[i], 
                                            mediums ) );
            
            

        }
        
        
        //////////////////////
        //Create devices
        devices = new HashMap<>();
        
        
        //////////////////////
        //Pins
        pinAssignments = new HashMap<>( pins.length );
        //For pinAssignments, row 2, is the pin index number not the pin number as seen on data sheets.
        //So it starts from 0 as opposed to 1, and refers to the position in variable pins.
        
        
        //Core 0
        pins[0]     = cores.get("0").getPin("PA0" );
        pins[1]     = cores.get("0").getPin("PA1" );
        pins[2]     = cores.get("0").getPin("PA2" );
        
        addPin(pinAssignments, 0,   "C0_0",     "PA0");
        addPin(pinAssignments, 1,   "C0_1",     "PA1");
        addPin(pinAssignments, 2,   "C0_2",     "PA2");
        
        pins[3]     = cores.get("0").getPin(  11  );
        pins[4]     = cores.get("0").getPin(  12  );
        pins[5]     = cores.get("0").getPin(  13  );
        pins[6]     = cores.get("0").getPin(  17  );
        pins[7]     = cores.get("0").getPin("INT6");
        pins[8]     = cores.get("0").getPin(  31  );
        pins[9]     = cores.get("0").getPin(  29  );
        pins[10]    = cores.get("0").getPin(  10  );
        pins[11]    = cores.get("0").getPin("PA5" );
        pins[12]    = cores.get("0").getPin("PA6" );
        
        addPin(pinAssignments, 3,   "C0_3",     "11");
        addPin(pinAssignments, 4,   "C0_4",     "12");
        addPin(pinAssignments, 5,   "C0_5",     "13");
        addPin(pinAssignments, 6,   "C0_6",     "17");
        addPin(pinAssignments, 7,   "C0_7",     "INT6");
        addPin(pinAssignments, 8,   "C0_8",     "31");
        addPin(pinAssignments, 9,   "C0_9",     "29");
        addPin(pinAssignments, 10,  "C0_10",    "10");
        addPin(pinAssignments, 11,  "C0_11",    "PA5");
        addPin(pinAssignments, 12,  "C0_12",    "PA6");    
        
        //For LEDS
        int pin_counter = 13;
        for ( int i=1; i < number_of_cores; i++ )
        {
            pins[pin_counter]     = cores.get( Integer.toString(i) ).getPin("PA0" );
            addPin(pinAssignments, pin_counter++,  "C" + i + "_0",     "PA0_" + i);

            pins[pin_counter]     = cores.get( Integer.toString(i) ).getPin("PA1" );
            addPin(pinAssignments, pin_counter++,  "C" + i + "_1",     "PA1_" + i);
        
            pins[pin_counter]     = cores.get( Integer.toString(i) ).getPin("PA2" );
            addPin(pinAssignments, pin_counter++,  "C" + i + "_2",     "PA2_" + i);       
        }
    }

    /**
     * The <code>installIOReg()</code> method installs an IO register with the specified name. The register
     * layout for this microcontroller is used to get the address of the register (if it exists) and
     * install the <code>ActiveRegister</code> object into the correct place.
     * @param name the name of the IO register as a string
     * @param reg the register to install
     */
/*    
    protected ActiveRegister installIOReg(String name, ActiveRegister reg) {
        interpreter.installIOReg(properties.getIOReg(name), reg);
    return reg;
    }
*/
    
    /**
     * The <code>getIOReg()</code> method gets a reference to the active register currently installed for
     * the specified name. The register layout for this microcontroller is used to get the correct address.
     * @param name the name of the IO register as a string
     * @return a reference to the active register object if it exists
     */
/*    
    protected ActiveRegister getIOReg(String name) {
        return interpreter.getIOReg(properties.getIOReg(name));
    }
*/
    
    /**
     * The <code>getDevice()</code> method is used to get a reference to an internal device with the given name.
     * For example, the ADC device will be under the name "adc" and Timer0 will be under the name "timer0". This
     * is useful for external devices that need to connect to the input of internal devices.
     *
     * @param name the name of the internal device as a string
     * @return a reference to the internal device if it exists; null otherwise
     */
    
    public static void addPin(HashMap<String, Integer>  pinMap, 
                                int                     p, 
                                String                  n) 
    {
        pinMap.put(n, new Integer(p));
    }

    public static void addPin(HashMap<String, Integer>  pinMap, 
                                int                     p,
                                String                  n1, 
                                String                  n2) 
    {
        Integer i = new Integer(p);
        pinMap.put(n1, i);
        pinMap.put(n2, i);
    }

    public static void addPin(HashMap<String, Integer>  pinMap,
                                int                     p,
                                String                  n1,
                                String                  n2, 
                                String                  n3) 
    {
        Integer i = new Integer(p);
        pinMap.put(n1, i);
        pinMap.put(n2, i);
        pinMap.put(n3, i);
    }

    public static void addPin(HashMap<String, Integer>  pinMap, 
                                int                     p, 
                                String                  n1, 
                                String                  n2, 
                                String                  n3, 
                                String                  n4) 
    {
        Integer i = new Integer(p);
        pinMap.put(n1, i);
        pinMap.put(n2, i);
        pinMap.put(n3, i);
        pinMap.put(n4, i);
    }

    public static void addInterrupt(HashMap<String, Integer> iMap, String n, int i) {
        iMap.put(n, new Integer(i));
    }
    
    /**
     * The <code>getProperties()</code> method gets a reference to the microcontroller properties for this
     * microcontroller instance.
     * @return a reference to the microcontroller properties for this instance
     */
/*
    public MCUProperties getProperties() {
        return properties;
    }
*/
/*    
    @Override
    public void Synchronizers_Start()
    {
        for (Synchronizer synch : synchs)
            synch.start();
    }
    
    @Override
    public void Synchronizers_Pause()
    {
        for (Synchronizer synch : synchs)
            synch.pause();
    }
    
    @Override
    public void Synchronizers_Stop()
    {
        for (Synchronizer synch : synchs)
            synch.stop();
    }    
    
    @Override
    public void Synchronizers_Join() throws InterruptedException
    {
        for (Synchronizer synch : synchs)
            synch.join();
    }      
*/ 

    public Synchronizer[] Get_Synchronizers_For_Core_ID( String core_ID )
    {
        return synchs; //Because every core is connected to every other core
    }
}
