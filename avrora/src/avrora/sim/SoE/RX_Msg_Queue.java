/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package avrora.sim.SoE;

import avrora.sim.mcu.AtmelInternalDevice;
import avrora.sim.mcu.AtmelMicrocontroller;
import avrora.sim.clock.Clock;
import java.util.LinkedList;

/**
 * Serial Peripheral Interface. Used on the <code>Mica2</code> platform for radio communication.
 *
 * @author Daniel Lee, Simon Han
 */
public class RX_Msg_Queue extends AtmelInternalDevice //implements InterruptTable.Notification
{
    public final short                                                  MSG_SIZE        = 32; //[Byte]
    private final short                                                 QUEUE_LENGTH    = 54; //pcs
    
    private final Receiver                                              receiver;      
    private final LinkedList<Message_Queue_Medium.Receiver.Msg_Entry>   msg_queue       = new LinkedList<>();
    
  
    
    public class Receiver extends Message_Queue_Medium.Receiver 
    {
        public Receiver(Message_Queue_Medium medium, 
                         Clock               clock,
                         byte                TXRX_ID_on_medium) 
        {
            super( medium, clock, TXRX_ID_on_medium );
        }
        
        @Override
        public Msg_Entry Get_Msg_Entry()
        {
            for ( Msg_Entry msg_entry : msg_queue )
                if ( msg_entry.msg_entry_state == Msg_Entry.Msg_Entry_States.EMPTY_STATE )
                    return msg_entry;
            
            return null;
        }
    }
            
    
    public RX_Msg_Queue(AtmelMicrocontroller    mcu, 
                        Message_Queue_Medium    medium,
                        byte                    TXRX_ID_on_medium) 
    {
        super("HWRXMsgQueue", mcu);
        
        //Create msg queue
        for ( short i = 0; i < QUEUE_LENGTH; i++)
            msg_queue.add( new Message_Queue_Medium.Receiver.Msg_Entry(new byte[MSG_SIZE]) );
        
        receiver = new RX_Msg_Queue.Receiver( medium, 
                                mcu.getSimulator().getClock(),
                                TXRX_ID_on_medium);
    }

    
    public Receiver Get_Receiver()
    {
        return receiver;    
    }
   
    /**
     * Data 
     */
    public byte read_data_byte(int byte_idx) 
    {
        return msg_queue.getFirst().buffer[byte_idx];
    }        
        
    public void write_data_byte(byte val, int byte_idx) 
    {
        msg_queue.getFirst().buffer[byte_idx] = val;
    }    
    
    
    /**
     * Msg source
     */
    public byte get_msg_ID_byte() 
    {
        return msg_queue.getFirst().TXRX_ID_on_medium;
    }
    
    public int get_msg_ID() 
    {
        int ID = get_msg_ID_byte();
            
        if (ID < 0)
            ID += 256;
            
        return ID;
    }
     
    
    /**
     * Control 
     */
    public void pop_first() 
    {
//        if ( devicePrinter != null )
//            devicePrinter.println( "HW TX msg queue control register: wrote " + StringUtil.toMultirepString(val, 8) );
            
        msg_queue.pop();
        msg_queue.push( new Message_Queue_Medium.Receiver.Msg_Entry(new byte[MSG_SIZE]) );
    }

    
    /**
     * Status 
     */
    public boolean msg_ready() 
    {
        return msg_queue.getFirst().msg_entry_state == Message_Queue_Medium.Receiver.Msg_Entry.Msg_Entry_States.DONE_STATE;
    }        

}

