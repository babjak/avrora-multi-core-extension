package avrora.sim.platform;

import avrora.sim.SoE.SoC_MultiCore;
import avrora.core.Program;
import avrora.sim.Simulation;
import avrora.sim.Simulator;
import avrora.sim.clock.ClockDomain;
import avrora.sim.clock.Synchronizer;
import avrora.sim.mcu.*;
import avrora.sim.platform.sensors.AccelSensor;
import avrora.sim.platform.sensors.AccelSensorPower;
import avrora.sim.platform.sensors.LightSensor;
import avrora.sim.platform.sensors.SensorBoard;
import avrora.sim.radio.*;
import cck.text.Terminal;

/**
 * The <code>MicaZ</code> class is an implementation of the <code>Platform</code> interface that represents
 * both a specific microcontroller and the devices connected to it. This implementation therefore uses the
 * ATMega128L microcontroller and uses LED and Radio devices, etc. The MicaZ class differs from Mica2 in that
 * the CC2420 radio implementation is installed instead of the CC1000.
 *
 */
public class MicaZ_MultiCore extends Platform {

    private static final int                    MAIN_HZ = 7372800;
//    private final System_Of_Elements_Interface  SoC;
    private final SoC_MultiCore                 SoC;

    public static class Factory implements PlatformFactory 
    {
        public  short   NUMBER_OF_CORES = 3; //Default value of three
        public  boolean AE_CORE_FLAG    = false;
        
        public  byte    DEST_CORE_ID    = 1;
        public  byte    DEST_COMP_ID    = 0;
        public  byte    DEST_INTERF_ID  = 0;
        public  byte    DEST_FUNC_ID    = 0;
        
        /**
         * The <code>newPlatform()</code> method is a factory method used to create new instances of the
         * <code>Mica2</code> class.
         * @param id the integer ID of the node
         * @param sim the simulation
         * @param p the program to load onto the node @return a new instance of the <code>Mica2</code> platform
         */
        @Override
        public Platform newPlatform(int id, Simulation sim, Program p) {
            Program[] progs = new Program[NUMBER_OF_CORES];
            progs[0] = p;
            
            return newPlatform(id, sim, progs);
        }
        
        @Override
        public Platform newPlatform(int id, Simulation sim, Program[] progs) {
            ClockDomain[] cds = new ClockDomain[NUMBER_OF_CORES];
            
            for ( int i = 0; i < NUMBER_OF_CORES; i++ )
            {
                ClockDomain cd;
                
                cd = new ClockDomain(MAIN_HZ);
                cd.newClock("external", 32768);
                
                cds[i] = cd;
            }                
            
            return new MicaZ_MultiCore( new SoC_MultiCore( id, 
                                                            sim, 
                                                            progs, 
                                                            cds, 
                                                            NUMBER_OF_CORES,
                                                            AE_CORE_FLAG,
                                                            DEST_CORE_ID,
                                                            DEST_COMP_ID,
                                                            DEST_INTERF_ID,
                                                            DEST_FUNC_ID ) );
        }
        
        @Override
        public short Get_Number_Of_Cores() 
        {
            return NUMBER_OF_CORES;
        }
        
        @Override
        public short Get_Number_Of_Synchronizers() 
        {
            return NUMBER_OF_CORES; //One synchronizer and medium for each core
        }        
    }

    protected CC2420Radio   radio;
    protected SensorBoard   sensorboard;
    protected ExternalFlash externalFlash;
    protected LightSensor   lightSensor;
    protected AccelSensor   accelXSensor;
    protected AccelSensor   accelYSensor;
    protected LED.LEDGroup  ledGroup;    
    
//    private MicaZ_MultiCore(System_Of_Elements_Interface SoC) 
    private MicaZ_MultiCore(SoC_MultiCore SoC) 
    {
        super( SoC.Get_Cores() );
        this.SoC = SoC;
        
        addDevices();
    }

    /*
    @Override
    public void Synchronizers_Start()
    {
        SoC.Synchronizers_Start();
    }
    
    @Override
    public void Synchronizers_Pause()
    {
        SoC.Synchronizers_Pause();
    }    
    
    @Override
    public void Synchronizers_Stop()
    {
        SoC.Synchronizers_Stop();
    }    
    
    @Override
    public void Synchronizers_Join() throws InterruptedException
    {
        SoC.Synchronizers_Join();
    }
    */ 
    
    @Override
    public String Get_ID_Of_Core_With_RF()
    {
        return "0";
    }

    @Override
    public Synchronizer[] Get_Synchronizers_For_Core_ID( String core_ID )
    {
        return SoC.Get_Synchronizers_For_Core_ID( core_ID );
    }    
    
    
    public class Dummy_SPI_Device implements SPIDevice 
    {
        @Override
        public SPI.Frame exchange(SPI.Frame frame) 
        {
            return SPI.newFrame((byte) 0);
        }

        @Override
        public void connect(SPIDevice d) 
        {
            // do nothing.
        }
    }    
    
    
    /**
     * The <code>addDevices()</code> method is used to add the external (off-chip) devices to the
     * platform. For the mica2, these include the LEDs, the radio, and the sensor board.
     */
    protected void addDevices() 
    {
        //Core 0
        AtmelMicrocontroller    amcu    = (AtmelMicrocontroller) cores.get("0");
        Simulator               sim     = amcu.getSimulator();
        
        LED yellow  = new LED( sim, Terminal.COLOR_YELLOW,   "Yellow");
        LED green   = new LED( sim, Terminal.COLOR_GREEN,    "Green");
        LED red     = new LED( sim, Terminal.COLOR_RED,      "Red");        

        ledGroup = new LED.LEDGroup( sim, new LED[] { yellow, green, red } );
        addDevice("leds", ledGroup);
       
        SoC.getPin("PA0" ).connectOutput(yellow);
        SoC.getPin("PA1" ).connectOutput(green);
        SoC.getPin("PA2" ).connectOutput(red);

        // install the new CC2420 radio
        radio = new CC2420Radio( amcu, MAIN_HZ * 2 );
        SoC.getPin( "11" ).connectOutput( radio.SCLK_pin  );
        SoC.getPin( "12" ).connectOutput( radio.MOSI_pin  );
        SoC.getPin( "13" ).connectInput ( radio.MISO_pin  );
        SoC.getPin( "17" ).connectInput ( radio.FIFO_pin  );
        SoC.getPin("INT6").connectInput ( radio.FIFOP_pin );
        SoC.getPin( "31" ).connectInput ( radio.CCA_pin   );
        SoC.getPin( "29" ).connectInput ( radio.SFD_pin   );
        SoC.getPin( "10" ).connectOutput( radio.CS_pin    );
        SoC.getPin("PA5" ).connectOutput( radio.VREN_pin  );
        SoC.getPin("PA6" ).connectOutput( radio.RSTN_pin  );
        ADC adc = (ADC) amcu.getDevice("adc");
        adc.connectADCInput(radio.adcInterface, 0);
        SPI spi = (SPI) amcu.getDevice("spi");
        spi.connect(radio.spiInterface);
        addDevice("radio", radio);        
        // install the input capture pin.
        Timer16Bit timer1 = (Timer16Bit) amcu.getDevice("timer1");
        radio.setSFDView( timer1.getInputCapturePin() );

        // sensor board
        sensorboard = new SensorBoard( sim );
        // external flash
        externalFlash = new ExternalFlash( amcu, 2048, 264 );
        // acceleration sensors
        AccelSensorPower asp = new AccelSensorPower( amcu, "PC4");
        accelXSensor = new AccelSensor( amcu, 3, asp);
        addDevice("accelx-sensor", accelXSensor);
        accelYSensor = new AccelSensor( amcu, 4, asp);
        addDevice("accely-sensor", accelYSensor);
        // light sensor
        lightSensor = new LightSensor( amcu, 1, "PE5");
        addDevice("light-sensor", lightSensor);  
        
        
        for ( int i=1; i < SoC.Get_Number_Of_Cores(); i++ )
        {
            amcu    = (AtmelMicrocontroller) cores.get( Integer.toString(i) );
            sim     = amcu.getSimulator();
        
            yellow = new LED( sim, Terminal.COLOR_YELLOW,   "Yellow");
            green  = new LED( sim, Terminal.COLOR_GREEN,    "Green");
            red    = new LED( sim, Terminal.COLOR_RED,      "Red");        

            ledGroup = new LED.LEDGroup( sim, new LED[] { yellow, green, red } );
            addDevice("leds" + i, ledGroup);
       
            SoC.getPin("PA0_" + i ).connectOutput( yellow );
            SoC.getPin("PA1_" + i ).connectOutput( green );
            SoC.getPin("PA2_" + i ).connectOutput( red );
        
            if (amcu instanceof IP_Core)
                continue;
            
            //HACK Dummy SPI device 
            Dummy_SPI_Device dummy_SPI_device = new Dummy_SPI_Device();
            spi = (SPI) amcu.getDevice("spi");
            spi.connect( dummy_SPI_device );
            addDevice("SPI_radio_dummy_" + i, dummy_SPI_device); 
        }
    }
}
