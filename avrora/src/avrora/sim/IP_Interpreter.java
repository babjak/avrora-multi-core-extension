/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package avrora.sim;

import avrora.arch.AbstractInstr;
import avrora.core.Program;
import avrora.sim.mcu.MCUProperties;


/**
 * The <code>LegacyInterpreter</code> class is largely generated from the instruction specification. The
 * framework around the generated code (utilities) has been written by hand, but most of the code for each
 * instruction is generated. Therefore it is not recommended to edit this code extensively.
 *
 * @author Ben L. Titzer
 */
public class IP_Interpreter extends Interpreter {
    protected int cyclesConsumed;

    protected long delayCycles;

    protected boolean shouldRun;

    protected boolean sleeping;

    protected final StateImpl state;
    
    
    public static final Factory FACTORY = new Factory();

    public static class Factory extends InterpreterFactory 
    {
        @Override
        public Interpreter newInterpreter(Simulator         s, 
                                            Program         p, 
                                            MCUProperties   pr) 
        {
            return new IP_Interpreter(s, pr);
        }
    }
  
    
    public class StateImpl implements State 
    {
        /**
         * The <code>getSimulator()</code> method returns the simulator associated with this state
         * instance.
         * @return a reference to the simulator associated with this state instance.
         */
        public Simulator getSimulator() {
            return simulator;
        }

        /**
         * The <code>getSP()</code> method reads the current value of the stack pointer. Since the stack pointer
         * is stored in two IO registers, this method will cause the invocation of the <code>.read()</code> method
         * on each of the <code>IOReg</code> objects that store these values.
         *
         * @return the value of the stack pointer as a byte address
         */
        public int getSP() {
            return 0;
        }

        /**
         * The <code>getPC()</code> retrieves the current program counter.
         *
         * @return the program counter as a byte address
         */
        public int getPC() {
            return 0;
        }

        /**
         * The <code>getInstr()</code> can be used to retrieve a reference to the <code>LegacyInstr</code> object
         * representing the instruction at the specified program address. Care should be taken that the address in
         * program memory specified does not contain data. This is because Avrora does have a functioning
         * disassembler and assumes that the <code>LegacyInstr</code> objects for each instruction in the program are
         * known a priori.
         *
         * @param address the byte address from which to read the instruction
         * @return a reference to the <code>LegacyInstr</code> object representing the instruction at that address in
         *         the program
         */
        public AbstractInstr getInstr(int address) {
            return null;
        }

        /**
         * The <code>getCycles()</code> method returns the clock cycle count recorded so far in the simulation.
         *
         * @return the number of clock cycles elapsed in the simulation
         */
        public long getCycles() {
            return clock.getCount();
        }
    }
    
    
    /**
     * The constructor for the <code>BaseInterpreter</code> class initializes the node's flash,
     * SRAM, general purpose registers, IO registers, and loads the program onto the flash. It
     * uses the <code>MicrocontrollerProperties</code> instance to configure the interpreter
     * such as the size of flash, SRAM, and location of IO registers.
     * @param simulator the simulator instance for this interpreter
     * @param p the program to load onto this interpreter instance
     * @param pr the properties of the microcontroller being simulated
     */
    protected IP_Interpreter(Simulator      simulator, 
                            MCUProperties   pr) 
    {
        super(simulator);
        
        
        // Dummy inits
        interrupts = new InterruptTable(this, pr.num_interrupts);
        state = new StateImpl();
        
        sleeping = false;
    }

    public void start() 
    {
        shouldRun = true;
        runLoop();
    }

    public void stop() 
    {
        shouldRun = false;
        innerLoop = false;
    }

    public State getState() 
    {
        return state;
    }

    /**
     * The <code>advanceClock()</code> method advances the clock by the specified number of cycles. It SHOULD NOT
     * be used externally. It also clears the <code>cyclesConsumed</code> variable that is used to track the
     * number of cycles consumed by a single instruction.
     * @param delta the number of cycles to advance the clock
     */
    protected void advanceClock(long delta) 
    {
        clock.advance(delta);
        cyclesConsumed = 0;
    }

    /**
     * The <code>delay()</code> method is used to add some delay cycles before the next instruction is executed.
     * This is necessary because some devices such as the EEPROM actually delay execution of instructions while
     * they are working
     * @param cycles the number of cycles to delay the execution
     */
    protected void delay(long cycles) 
    {
        innerLoop = false;        
        delayCycles += cycles;
    }

    /**
     * The <code>commit()</code> method is used internally to commit the results of the instructiobn just executed.
     * This should only be used internally.
     */
    protected void commit() {
        clock.advance(cyclesConsumed);
        cyclesConsumed = 0;
    }


    protected void runLoop() {

        cyclesConsumed = 0;

        while (shouldRun) 
        {
            // TODO: would a "mode" and switch be faster than several branches?
            if (delayCycles > 0) 
            {
                advanceClock(delayCycles);
                delayCycles = 0;
            }

            if (sleeping)
                sleepLoop();
            else 
                fastLoop();
        }
    }

    public int step() {
        // process any delays
        if (delayCycles > 0) {
            advanceClock(1);
            delayCycles--;
            return 1;
        }

        // are we sleeping?
        if ( sleeping ) {
            advanceClock(1);
            return 1;
        }

        return stepInstruction();
    }

    private int stepInstruction() 
    {
        int cycles;

        // TODO: DO stuff and increase cycle accordingly
        cyclesConsumed += 1;
        
        // NOTE: commit() might be called twice, but this is ok
        cycles = cyclesConsumed;
        commit();
        
        return cycles;
    }

    private void sleepLoop() 
    {
        innerLoop = true;
        
        while (innerLoop)
            clock.skipAhead();
    }

    private void fastLoop() 
    {
        innerLoop = true;
        
        while (innerLoop) 
        {    
            // TODO: DO stuff and increase cycle accordingly
            cyclesConsumed += 1; 
        
            commit();
        }
    }


    /**
     * send the node to sleep
     */
    public void enterSleepMode() {
        sleeping = true;
        innerLoop = false;
        simulator.getMicrocontroller().sleep();
    }

    /**
     * time to wake up
     */
    public void leaveSleepMode() {
        sleeping = false;
        innerLoop = false;
        advanceClock( simulator.getMicrocontroller().wakeup() );
    }
    
    
    /**
     * The <code>insertProbe()</code> method is used internally to insert a probe on a particular instruction.
     * @param p the probe to insert on an instruction
     * @param addr the address of the instruction on which to insert the probe
     */
    protected void insertProbe(Simulator.Probe p, int addr) 
    {
    }

    /**
     * The <code>insertExceptionWatch()</code> method registers an </code>ExceptionWatch</code> to listen for
     * exceptional conditions in the machine.
     *
     * @param watch The <code>ExceptionWatch</code> instance to add.
     */
    protected void insertErrorWatch(Simulator.Watch watch) 
    {
    }

    /**
     * The <code>insertProbe()</code> method allows a probe to be inserted that is executed before and after
     * every instruction that is executed by the simulator
     *
     * @param p the probe to insert
     */
    protected void insertProbe(Simulator.Probe p) 
    {
        innerLoop = false;        
    }

    /**
     * The <code>removeProbe()</code> method is used internally to remove a probe from a particular instruction.
     * @param p the probe to remove from an instruction
     * @param addr the address of the instruction from which to remove the probe
     */
    protected void removeProbe(Simulator.Probe p, int addr) 
    {
        innerLoop = false;        
    }

    /**
     * The <code>removeProbe()</code> method removes a probe from the global probe table (the probes executed
     * before and after every instruction). The comparison used is reference equality, not
     * <code>.equals()</code>.
     *
     * @param b the probe to remove
     */
    public void removeProbe(Simulator.Probe b) 
    {
    }

    /**
     * The <code>insertWatch()</code> method is used internally to insert a watch on a particular memory location.
     * @param p the watch to insert on a memory location
     * @param data_addr the address of the memory location on which to insert the watch
     */
    protected void insertWatch(Simulator.Watch p, int data_addr) 
    {
    }

    /**
     * The <code>removeWatch()</code> method is used internally to remove a watch from a particular memory location.
     * @param p the watch to remove from the memory location
     * @param data_addr the address of the memory location from which to remove the watch
     */
    protected void removeWatch(Simulator.Watch p, int data_addr) 
    {
    }
}
