package avrora.sim.mcu;

import java.nio.ByteBuffer;
import avrora.arch.avr.AVRProperties;
import avrora.core.Program;
import avrora.sim.*;
import avrora.sim.SoE.Message_Queue_Medium;
import avrora.sim.SoE.RX_Msg_Queue;
import avrora.sim.SoE.TX_Msg_Queues;
import avrora.sim.clock.ClockDomain;
import avrora.sim.energy.Energy;
import static avrora.sim.mcu.AtmelMicrocontroller.addPin;
import java.util.HashMap;

public class IP_Core extends AtmelMicrocontroller //For conveniance only, not really a microcontroller
{
    public static final int ATMEGA128_IOREG_SIZE    = 0; 
    public static final int ATMEGA128_SRAM_SIZE     = 0;
    public static final int ATMEGA128_FLASH_SIZE    = 0;
    public static final int ATMEGA128_EEPROM_SIZE   = 0;
    public static final int ATMEGA128_NUM_PINS      = 3;
    public static final int ATMEGA128_NUM_INTS      = 0;

    public static final int MODE_IDLE       = 1;
    public static final int MODE_ADCNRED    = 2;
    public static final int MODE_POWERDOWN  = 3;
    public static final int MODE_POWERSAVE  = 4;
    public static final int MODE_RESERVED1  = 5;
    public static final int MODE_RESERVED2  = 6;
    public static final int MODE_STANDBY    = 7;
    public static final int MODE_EXTSTANDBY = 8;
    
    protected static final String[] idleModeNames = 
    {
        "Active",
        "Idle",
        "ADC Noise Reduction",
        "Power Down",
        "Power Save",
        "RESERVED 1",
        "RESERVED 2",
        "Standby",
        "Extended Standby"
    };

    //power consumption of each mode (ATMEGA128L)
    private static final double[] modeAmpere = 
    {
        0.0075667,
        0.0033433,
        0.0009884,
        0.0001158,
        0.0001237,
        0.0,
        0.0,
        0.0002356,
        0.0002433
    };


    protected static final int[] wakeupTimes = 
    {
        0, 0, 0, 1000, 1000, 0, 0, 6, 6
    };

    private static final int[][] transitionTimeMatrix  = FiniteStateMachine.buildBimodalTTM(idleModeNames.length, 
                                                                                            0, 
                                                                                            wakeupTimes, 
                                                                                            new int[wakeupTimes.length] );
    

    /**
     * The <code>props</code> field stores a static reference to a properties
     * object shared by all of the instances of this microcontroller. This object
     * stores the IO register size, SRAM size, pin assignments, etc.
     */
    public static final AVRProperties props;

    static 
    {
        // statically initialize the pin assignments for this microcontroller
        HashMap<String, Integer> pinAssignments         = new HashMap<>(1);
        HashMap<String, Integer> interruptAssignments   = new HashMap<>(1);

        addPin(pinAssignments, 0,  "PA2");
        addPin(pinAssignments, 1,  "PA1");
        addPin(pinAssignments, 2,  "PA0");        
        
        RegisterLayout rl = new RegisterLayout(ATMEGA128_IOREG_SIZE, 8);

        //No interupts - at least for now
        //addInterrupt(interruptAssignments, "RESET",         1);
        
        props = new AVRProperties(ATMEGA128_IOREG_SIZE, // number of io registers
                                ATMEGA128_SRAM_SIZE,    // size of sram in bytes
                                ATMEGA128_FLASH_SIZE,   // size of flash in bytes
                                ATMEGA128_EEPROM_SIZE,  // size of eeprom in bytes
                                ATMEGA128_NUM_PINS,     // number of pins
                                ATMEGA128_NUM_INTS,     // number of interrupts
                                new ReprogrammableCodeSegment.Factory(ATMEGA128_FLASH_SIZE, 7),
                                pinAssignments,         // the assignment of names to physical pins
                                rl,                     // the assignment of names to IO registers
                                interruptAssignments);
    }

    private Message_Queue_Medium    RX_medium;
    private Message_Queue_Medium[]  mediums;
    
    private RX_Msg_Queue            RX_msg_queue;
    private TX_Msg_Queues           TX_msg_queues;
    
    private byte                    dest_core_ID;
    private byte                    dest_comp_ID;
    private byte                    dest_interf_ID;
    private byte                    dest_func_ID; 
    

    /**
    * The <code>Ticker</code> class implements a Simulator Event call Ticker
    * that is fired  when a timed event occurs within the simulator in order
    * to model a mote reception
    */
    protected class Ticker implements Simulator.Event 
    {
        private Event_Class event;
        
        ////////////////////////////////////////////////////////////////////
        public Ticker(Event_Class event)
        {
            this.event = event;
        }
        
        ////////////////////////////////////////////////////////////////////
        public void fire() 
        {
            AE_Event_Callback(event); 
        }

    }  
    
    
//    public static final int NUMBER_OF_EVENT_CHANNELS = 2;
    int                         last_AE_post_time;
    
    static class Event_Class
    {
        public int              frame_time_ms;	
        public short            log_quality_idx_m[];	// milli log quality idx  
        public short            time_diff_us[];		// time difference of event relative to event time on first channel
        
        public byte[] Get_Byte_Buffer()
        {
            byte        buf[]   = new byte[ 4 + log_quality_idx_m.length * 2 + time_diff_us.length * 2 ];
            ByteBuffer  bb      = ByteBuffer.wrap(buf);
            
            bb.putInt(frame_time_ms);
            for (short item : log_quality_idx_m)
                bb.putShort(item);
            for (short item : time_diff_us)
                bb.putShort(item);
            //Set inner counter to 0
            //bb.flip(); // not needed here
            
            return buf;
        }
    }

    static final Event_Class[] events = new Event_Class[] {
        new Event_Class() {{ frame_time_ms = 1100; log_quality_idx_m = new short[] {-3000, -3000}; time_diff_us = new short[] {250}; }}
        };
    
    
    ////////////////////////////////////////////////////////////////////////////
    public IP_Core(int                      node_id,
                    String                  core_id,
                    byte                    core_number,
                    Simulation              sim, 
                    ClockDomain             cd, 
                    Program                 p,
                    Message_Queue_Medium    RX_medium,
                    Message_Queue_Medium[]  mediums,
                    byte                    dest_core_ID,
                    byte                    dest_comp_ID,
                    byte                    dest_interf_ID,
                    byte                    dest_func_ID) 
    {
        super(cd, props, new FiniteStateMachine(cd.getMainClock(), 
                                        MODE_ACTIVE, 
                                        idleModeNames, 
                                        transitionTimeMatrix));
        
        this.RX_medium  = RX_medium;
        this.mediums    = mediums;
        
        this.dest_core_ID   = dest_core_ID;
        this.dest_comp_ID   = dest_comp_ID;
        this.dest_interf_ID = dest_interf_ID;
        this.dest_func_ID   = dest_func_ID;
        
        simulator   = sim.createSimulator(node_id, core_id, IP_Interpreter.FACTORY, this, p);
        
        installPins();
        installDevices(core_number);
        new Energy("CPU", 
                modeAmpere, 
                sleepState, 
                simulator.getEnergyControl() );
        
        Post_AE_Events(); 
    }

    
    ////////////////////////////////////////////////////////////////////////////
    protected void Post_AE_Events() 
    {
        for (Event_Class event : events)
            simulator.getClock().insertEvent( new Ticker(event), simulator.getClock().millisToCycles(event.frame_time_ms) ); 
    }    
    

    ////////////////////////////////////////////////////////////////////////////
    protected void AE_Event_Callback(Event_Class event) 
    {
        // Send message via queue-based messaging framework
        
        // 1. Allocate message buffer
        boolean allocation_success = false;
        for ( int i = 0; i < 10; i++ )
        {
            allocation_success = TX_msg_queues.get_successfull_allocation();
            
            if (allocation_success == true)
                break;
        }
        
        if (allocation_success == false)
            return;

        // 2. Write message bytes to buffer
        byte[] data = event.Get_Byte_Buffer();
        
        int byte_idx_cnt = 0;
        
        TX_msg_queues.write_data_byte( dest_comp_ID,    byte_idx_cnt++ );
        TX_msg_queues.write_data_byte( dest_interf_ID,  byte_idx_cnt++ );
        TX_msg_queues.write_data_byte( dest_func_ID,    byte_idx_cnt++ );
        
        for (byte data_byte : data)
            TX_msg_queues.write_data_byte( data_byte, byte_idx_cnt++ );
        
        // 3. Send buffer contents
        TX_msg_queues.write_to_dest(dest_core_ID);
    }    
    
    
    ////////////////////////////////////////////////////////////////////////////
    protected void installPins() 
    {
        for (int cntr = 0; cntr < properties.num_pins; cntr++)
            pins[cntr] = new ATMegaFamily.Pin(cntr);
    }

    
    ////////////////////////////////////////////////////////////////////////////
    protected void installDevices(byte core_number) 
    {
        RX_msg_queue = new RX_Msg_Queue( (AtmelMicrocontroller) this, RX_medium, core_number);
                
        TX_msg_queues = new TX_Msg_Queues( (AtmelMicrocontroller) this, mediums, core_number); 
        
        addDevice( RX_msg_queue );
        addDevice( TX_msg_queues );
    }

    
    protected int getSleepMode() 
    {
        return sleepState.getCurrentState();
    }
}
