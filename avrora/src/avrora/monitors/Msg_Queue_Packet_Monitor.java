package avrora.monitors;

import avrora.sim.Simulator;
import avrora.sim.SoE.RX_Msg_Queue;
import avrora.sim.SoE.TX_Msg_Queues;
import avrora.sim.SoE.Message_Queue_Medium;
import avrora.sim.mcu.AtmelMicrocontroller;
import avrora.sim.output.SimPrinter;
import cck.text.*;
import cck.util.Option;

import java.util.*;

/**
 * Packet monitor implementation. This class logs the number of packets, e.g. bytes sent and received.
 *
 * @author Olaf Landsiedel
 * @author Ben L. Titzer
 */
public class Msg_Queue_Packet_Monitor extends MonitorFactory {

    private static final int INITIAL_BUFFER_SIZE = 64;

    protected Option.Bool BITS = newOption("show-bits", false,
            "This option enables the printing of packet contents in bits rather than in bytes.");
    protected Option.Bool PACKETS = newOption("show-packets", true,
            "This option enables the printing of packets as they are transmitted.");


    protected List<Msg_Queue_Mon> monitors = new LinkedList<>();

    class Msg_Queue_Mon implements Monitor, Message_Queue_Medium.Msg_Queue_TX_Probe, Message_Queue_Medium.Msg_Queue_RX_Probe {
        
        class Buffer_Class
        {
            byte[]  buffer      = null;
            int     buffer_pos  = 0; 
            long    startCycle  = 0;
        }

        
        HashMap<Integer, Buffer_Class>  TX_buffers = new HashMap<>();
        HashMap<Integer, Buffer_Class>  RX_buffers = new HashMap<>();

        final Simulator                 simulator;
        final SimPrinter                printer;
        
        final boolean                   show_packets;
        final boolean                   show_bits;

        int                             bytesTransmitted;
        int                             packetsTransmitted;
        int                             bytesReceived;
        int                             packetsReceived;


        ////////////////////////////////////////////////////////////////////////
        Msg_Queue_Mon(Simulator s) 
        {
            simulator = s;
            
            TX_Msg_Queues    TX = (TX_Msg_Queues) ( (AtmelMicrocontroller) simulator.getMicrocontroller() ).getDevice( "HWTXMsgQueue" );
            RX_Msg_Queue     RX = (RX_Msg_Queue) ( (AtmelMicrocontroller) simulator.getMicrocontroller() ).getDevice( "HWRXMsgQueue" );
            
            if ( TX == null || RX == null )
            {
                printer = null;
                
                show_packets    = false;
                show_bits       = false;
                
                return;
            }
            
            TX_Msg_Queues.Transmitter[] transmitters = TX.Get_Transmitters();
            
            for (TX_Msg_Queues.Transmitter transmitter : transmitters)
                transmitter.insertProbe(this);

            RX.Get_Receiver().insertProbe(this);
           
            printer         = simulator.getPrinter();
            
            show_packets    = PACKETS.get();
            show_bits       = BITS.get();

            monitors.add(this);
        }

        
        ////////////////////////////////////////////////////////////////////////
        private void Append( HashMap<Integer, Buffer_Class> buffers, byte TX_ID_on_core, byte c ) 
        {
            Buffer_Class buffer_o = buffers.get( (int) TX_ID_on_core );
            
            if ( buffer_o == null )
            {
                buffer_o = new Buffer_Class();
                buffers.put( (int) TX_ID_on_core, buffer_o );
            }
            
            if (buffer_o.buffer == null) 
            {
                buffer_o.buffer     = new byte[INITIAL_BUFFER_SIZE];
                buffer_o.buffer_pos = 0;
            }
            else if (buffer_o.buffer.length == buffer_o.buffer_pos) 
            {
                byte[] newData = new byte[buffer_o.buffer.length * 2];
                System.arraycopy(buffer_o.buffer, 0, newData, 0, buffer_o.buffer.length);
                buffer_o.buffer = newData;
            }
            
            if ( buffer_o.buffer_pos == 0 ) 
                buffer_o.startCycle = simulator.getClock().getCount();
            
            buffer_o.buffer[buffer_o.buffer_pos++] = c;
        }

        
        ////////////////////////////////////////////////////////////////////////
        private void clear( HashMap<Integer, Buffer_Class> buffers, byte TX_ID_on_core ) 
        {
            Buffer_Class buffer_o = buffers.get( (int) TX_ID_on_core );
            
            if ( buffer_o == null )
                return;
            
            buffer_o.buffer_pos = 0;
        }

        
        ////////////////////////////////////////////////////////////////////////
        @Override
        public void Fire_Before_Buffer_Request_Transmit( byte TX_ID_on_core )
        {
            if ( printer == null )
                return;
            
            if (!show_packets) 
                return;
            
            String s = "----> Sending buffer request message to on-core TX: " + TX_ID_on_core + ".";
            StringBuffer buf = printer.getBuffer( s.length()+2 );
            Terminal.append( Terminal.COLOR_DEFAULT, buf, s );
        }
        
        
        ////////////////////////////////////////////////////////////////////////
        @Override
        public void Fire_After_Buffer_Response_Receive(  byte TX_ID_on_core, boolean buffer_granted )
        {
            if ( printer == null )
                return;
            
            if (!show_packets) 
                return;
            
            String s = "<====  Received buffer request response message on on-core TX: " + TX_ID_on_core + ".";
            StringBuffer buf = printer.getBuffer( s.length()+2 );
            Terminal.append( Terminal.COLOR_DEFAULT, buf, s );                       
        }
        
        
        ////////////////////////////////////////////////////////////////////////
        @Override
        public void Fire_Before_Transmit( byte TX_ID_on_core, byte val)
        {
            if ( printer == null )
                return;
            
            Append( TX_buffers, TX_ID_on_core, val );
            
            bytesTransmitted++;            
        }
        
        
        ////////////////////////////////////////////////////////////////////////
        @Override
        public void Fire_Before_Transmit_End( byte TX_ID_on_core )
        {
            if ( printer == null )
                return;

            Buffer_Class buffer_o = TX_buffers.get( (int) TX_ID_on_core );
            
            if ( buffer_o == null )
                return;
            
            if ( buffer_o.buffer_pos == 0 || buffer_o.buffer == null ) 
                return;
            
            packetsTransmitted++;
            
            if (show_packets) 
                printer.printBuffer( Render_Packet("----> ", buffer_o, 0, buffer_o.buffer_pos, " to on-core TX: " + TX_ID_on_core) );
            
            clear( TX_buffers, TX_ID_on_core );
        }        
        
        
        ////////////////////////////////////////////////////////////////////////
        @Override
        public void Fire_After_Buffer_Request_Receive( byte TXRX_ID_on_medium )
        {
            if ( printer == null )
                return;
            
            if (!show_packets) 
                return;
            
            String s = "<====  Received buffer request message from on-medium TX: " + TXRX_ID_on_medium + ".";
            StringBuffer buf = printer.getBuffer( s.length()+2 );
            Terminal.append( Terminal.COLOR_DEFAULT, buf, s );            
        }
        
        
        ////////////////////////////////////////////////////////////////////////
        @Override
        public void Fire_Before_Buffer_Response_Transmit( byte TXRX_ID_on_medium )
        {
            if ( printer == null )
                return;
            
            if (!show_packets) 
                return;
            
            String s = "----> Sending buffer request response message to on-medium TX: " + TXRX_ID_on_medium + ".";
            StringBuffer buf = printer.getBuffer( s.length()+2 );
            Terminal.append( Terminal.COLOR_DEFAULT, buf, s );
        }
        
        
        ////////////////////////////////////////////////////////////////////////
        @Override
        public void Fire_After_Receive( byte TXRX_ID_on_medium, byte val)
        {
            if ( printer == null )
                return;
            
            Append( RX_buffers, TXRX_ID_on_medium, val );
            
            bytesReceived++;
        }  
        
        
        ////////////////////////////////////////////////////////////////////////
        @Override
        public void Fire_After_Receive_End( byte TXRX_ID_on_medium )
        {
            if ( printer == null )
                return;

            Buffer_Class buffer_o = RX_buffers.get( (int) TXRX_ID_on_medium );
            
            if ( buffer_o == null )
                return;  
            
            if ( buffer_o.buffer_pos == 0 || buffer_o.buffer == null ) 
                return;

            packetsReceived++;
            
            if ( show_packets )
                printer.printBuffer( Render_Packet("<==== ", buffer_o, 0, buffer_o.buffer_pos, " from on-medium TX: " + TXRX_ID_on_medium) );

            clear( RX_buffers, TXRX_ID_on_medium );
        }

        
        ////////////////////////////////////////////////////////////////////////
        private StringBuffer Render_Packet(String           prefix,
                                            Buffer_Class    buffer_o,
                                            int             start, 
                                            int             end,
                                            String          postfix) 
        {
            int packetLen = end - start;
            StringBuffer buf = printer.getBuffer(3 * packetLen + 15);
            Terminal.append(Terminal.COLOR_BRIGHT_CYAN, buf, prefix);
            
            // real packet
            for ( int cntr = 0; cntr < packetLen; cntr++ ) 
            {
                Render_Byte( buf, Terminal.COLOR_DEFAULT, buffer_o.buffer[start + cntr] ); 
                if (cntr < packetLen - 1) 
                    buf.append('.');
            }
            
            buf.append( postfix + " " );
            
            appendTime( buf, buffer_o.startCycle );
            return buf;
        }
        
        
        ////////////////////////////////////////////////////////////////////////
        private void Render_Byte(StringBuffer   buf, 
                                int             color, 
                                byte            value) 
        {
            if (show_bits) 
            {
                for (int i = 7; i >= 0; i--) 
                {
                    boolean bit = (value >> i & 1) != 0;
                    Terminal.append(color, buf, bit ? "1" : "0");
                }
            } 
            else 
                Terminal.append( color, buf, StringUtil.toHex(value, 2) );
        }        
        
        
        ////////////////////////////////////////////////////////////////////////
        private void appendTime( StringBuffer buf, long startCycle ) 
        {
            long cycles = simulator.getClock().getCount() - startCycle;
            double ms = simulator.getClock().cyclesToMillis(cycles);
            
            buf.append("  ");
            buf.append( StringUtil.toFixedFloat((float)ms, 3) );
            buf.append(" ms");
        }        
        
        
        ////////////////////////////////////////////////////////////////////////
        @Override
        public void report() 
        {
            if ( printer == null )
                return;
               
            if ( monitors == null ) 
                return;
            
            TermUtil.printSeparator(Terminal.MAXLINE, "Message Queue Packet monitor results");
                
            Terminal.printGreen("Node     sent (b/p)          recv (b/p)");
                
            Terminal.nextln();
            TermUtil.printThinSeparator();
            
            for (Msg_Queue_Mon mon : monitors) 
            {
                if ( mon.simulator.getcoreID() == null )
                    Terminal.print( StringUtil.rightJustify(mon.simulator.getID(), 4) );
                else
                    Terminal.print( StringUtil.rightJustify( Integer.toString( mon.simulator.getID() ) + "-" + mon.simulator.getcoreID(), 4 ) );
                   
                Terminal.print(StringUtil.rightJustify(mon.bytesTransmitted, 10));
                Terminal.print(" / ");
                Terminal.print(StringUtil.leftJustify(mon.packetsTransmitted, 8));

                Terminal.print(StringUtil.rightJustify(mon.bytesReceived, 10));
                Terminal.print(" / ");
                Terminal.print(StringUtil.leftJustify(mon.packetsReceived, 8));
                
                Terminal.nextln();
            }
            
            monitors = null;
            Terminal.nextln();
        }
    }

    /**
     * create a new monitor
     */
    public Msg_Queue_Packet_Monitor() 
    {
        super("The message queue \"packet\" monitor tracks packets sent and received by cores on a SoC.");
    }

    /**
     * create a new monitor, calls the constructor
     *
     * @see MonitorFactory#newMonitor(Simulator)
     */
    @Override
    public Monitor newMonitor(Simulator s) 
    {
        return new Msg_Queue_Mon(s);
    }
}

